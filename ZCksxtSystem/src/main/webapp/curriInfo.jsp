<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<script>
$(function(){
	 $("#btncur").click(function(){
			$('#dgcur').datagrid('load',{
	  			name:$("#namecur").val()
	  		});
		});
	$('#dgcur').datagrid({
		rownumbers:true,
		singleSelect:true,
		pagination:true,
		toolbar:'#bbcur',
		pageSize:'10',
		fitColumns:true,
		fit:true,
		pageList:[5,10,15,20,35,40],
	    url:'CurriInfoServlet?op=select',
	    columns:[[
			{field:'curName',title:'科目名称',width:100,editor:"validatebox"},
			{field:'action',title:'操作',width:70,align:'center',
				formatter:function(value,row,index){
					if (row.editing){
						var s = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="saverowscur(this)">保存</a></button>';
						var a = '<span style="width:20px;"> </span>';
						var c = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="cancelrowscur(this)">取消</a></button>';
						return s+a+c;
					} else {
						var e = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="editrowscur(this)">修改</a></button>';
						var a = '<span style="width:20px;"> </span>';
						var d = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="deleterowscur(this,'+row.curId+')">删除</a></button>';
						return e+a+d;
					}
				}
			}
	    ]],
	 onEndEdit:function(index,row){
			$.ajax
		    ({
		    	url: "CurriInfoServlet",
				dataType: "text",
				type: "post",
				data:"op=update&id="+row.curId+"&name="+row.curName,
			    success:function(data){
				    if(data=="修改成功"){
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
			    	$('#dgcur').datagrid('load');
				}
		    });
		},
		onBeforeEdit:function(index,row){
			row.editing = true;
			$(this).datagrid('refreshRow', index);
		},
		onAfterEdit:function(index,rowData){
			rowData.editing = false;
			$(this).datagrid('refreshRow', index);
			id=rowData.userId;
		},
		onCancelEdit:function(index,row){
			row.editing = false;
			$(this).datagrid('refreshRow', index);
		}
	});
});
function getRowIndexscur(target){
	var tr = $(target).closest('tr.datagrid-row');
	return parseInt(tr.attr('datagrid-row-index'));
}
function editrowscur(target){
	$('#dgcur').datagrid('beginEdit', getRowIndexscur(target));
}
function deleterowscur(target,id){
	$.messager.confirm('删除','确定删除吗?',function(r){
		if (r){
			$('#dgcur').datagrid('deleteRow', getRowIndexscur(target));
			$.ajax({
		    	url: "CurriInfoServlet",
				dataType: "text",
				type: "post",
				data:"op=delete&id="+id,
			    success:function(data){
				    if(data=="删除成功"){
				    	$('#dgcur').datagrid('load');
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
				}
		    });
		}
	});
}
function saverowscur(target){
	$('#dgcur').datagrid('endEdit', getRowIndexscur(target));
}
function cancelrowscur(target){
	$('#dgcur').datagrid('cancelEdit', getRowIndexscur(target));
}
</script>
		<div id="bbcur">
		科目名称：
			<input name="namecur" id="namecur" class="easyui-textbox" prompt="请输入科目名称"  style="width:200px;height:34px;padding:10px">
			<a href="#" id="btncur" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a href="javaScript:addTabs('添加科目','addcurriInfo.jsp')" id="addcur" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">添加</a>
		</div>
	<table id="dgcur"></table>
</body>
</html>