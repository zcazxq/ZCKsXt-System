<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	 <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<script>
$(function(){
	$('#cccsss').combobox({
		editable:false,
		prompt:"请先选择试卷",
		url:'ScoreInfoServlet?op=selectsjidname',
		valueField:'paperId',
		textField:'paperName',
		onChange:function(){
			$('#scodg').datagrid('load',{
	  			name:$("#sconames").val(),
	  			sjid:$(this).val()
	  		});
		 }
	});
	 $("#scobtn").click(function(){
			$('#scodg').datagrid('load',{
	  			name:$("#sconames").val(),
	  			sjid:$("#cccsss").val()
	  		});
		});
	$('#scodg').datagrid({
		rownumbers:true,
		singleSelect:true,
		pagination:true,
		toolbar:'#scobb',
		pageSize:'10',
		fitColumns:true,
		fit:true,
		pageList:[5,10,15,20,35,40],
	    url:'ScoreInfoServlet?op=select',
	    columns:[[
			{field:'stuname',title:'学生姓名',width:100},
			{field:'papername',title:'试卷名称',width:100},
			{field:'score',title:'成绩',width:100}
	    ]],
	 onEndEdit:function(index,row){
			$.ajax
		    ({
		    	url: "ScoreInfoServlet",
				dataType: "text",
				type: "post",
				data:"op=update&id="+row.classId+"&name="+row.className,
			    success:function(data){
				    if(data=="修改成功"){
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
			    	$('#dg').datagrid('load');
				}
		    });
		},
		onBeforeEdit:function(index,row){
			row.editing = true;
			$(this).datagrid('refreshRow', index);
		},
		onAfterEdit:function(index,rowData){
			rowData.editing = false;
			$(this).datagrid('refreshRow', index);
			id=rowData.userId;
		},
		onCancelEdit:function(index,row){
			row.editing = false;
			$(this).datagrid('refreshRow', index);
		}
	});
});
function getscoRowIndexs(target){
	var tr = $(target).closest('tr.datagrid-row');
	return parseInt(tr.attr('datagrid-row-index'));
}
function scoeditrows(target){
	$('#scodg').datagrid('beginEdit', getscoRowIndexs(target));
}
function scodeleterows(target,id){
	$.messager.confirm('删除','确定删除吗?',function(r){
		if (r){
			$('#scodg').datagrid('deleteRow', getscoRowIndexs(target));
			$.ajax({
		    	url: "ScoreInfoServlet",
				dataType: "text",
				type: "post",
				data:"op=delete&id="+id,
			    success:function(data){
				    if(data=="删除成功"){
				    	$('#dg').datagrid('load');
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
				}
		    });
		}
	});
}
function scosaverows(target){
	$('#scodg').datagrid('endEdit', getscoRowIndexs(target));
}
function scocancelrows(target){
	$('#scodg').datagrid('cancelEdit', getscoRowIndexs(target));
}
</script>
		<div id="scobb">
		试卷名称：
		<input id="cccsss" name="cccsss" style="width:200px;height:34px;">
		学生姓名：
			<input name="sconames" id="sconames" class="easyui-textbox" prompt="请输入学生姓名（选填）"  style="width:200px;height:34px;padding:10px">
			<a href="#" id="scobtn" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
		</div>
	<table id="scodg"></table>
</body>
</html>