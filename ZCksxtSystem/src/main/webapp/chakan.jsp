<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>试卷预览</title>
    <link rel="stylesheet" href="js/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
    <link rel="icon" type="image/png" href="assets/i/favicon.png">
	<script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
	<script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
	<script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
    <style type="text/css">
        .testCon{padding: 5px 15px;}
        h4.tesTitle{color: #00B895;}
        .test-form-box{max-height: 660px;overflow: auto;}
        .assignment{margin: 30px 0 60px;text-align: center;}
        .assignment .btn{background: #00B895;width: 120px;font-size: 18px;border-color: #00B895;}
        #testForm .testCon:nth-child(odd){background: #eee;}
        label{font-weight: normal;}
        .jxz-title{text-align: justify;}
        .topic-answer{display: none;}
    </style>
</head>
<body>
    <div class="container">
        <div class="row clearfix"id="testArea">

        </div>
    </div>

<script type="text/javascript" src="js/test.js"></script>
<script type="text/javascript">
    $(function(){
        var sjid ="获取id";
        test(sjid);
    });
    function myYichu(sjid,tkid){
    	$.ajax({
	    	url: "QuestionInfoServlet",
			dataType: "text",
			type: "post",
			data:"op=updatesjstate&sjid="+sjid+"&tkid="+tkid,
		    success:function(data){
			    if(data=="移除成功"){
			    	
			    }else{
			    	$.messager.alert('温馨提示',data+",请检查后重试！");	
			    }
			    location.reload();
			}
	    });
    }
</script>
</body>
</html>