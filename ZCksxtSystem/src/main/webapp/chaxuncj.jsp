<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.dao.*,com.model.*,java.util.*"%>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>在线考试平台</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="assets/css/admin.css">
    <link rel="stylesheet" href="assets/css/app.css">
     <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
	<script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
    <script src="assets/js/amazeui.min.js"></script>
    <script src="assets/js/iscroll.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="assets/js/echarts.min.js"></script>
</head>
<style>
.combo-panel panel-body panel-body-noheader{
	width: 200px;
	height: 300px;
} 
</style>


<%
	StudentInfo ss=(StudentInfo)request.getSession().getAttribute("model");
%>
<body data-type="index" style="overflow: hidden;">


    <header class="am-topbar am-topbar-inverse admin-header">
        <div class="am-topbar-brand">
            <a href="javascript:;" class="tpl-logo">
                <img src="assets/img/logo.png" alt="">
            </a>
        </div>

        <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>

        <div class="am-collapse am-topbar-collapse" id="topbar-collapse">

            <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list tpl-header-list">
                
                 <li class="am-dropdown" data-am-dropdown data-am-dropdown-toggle>
                    <a class="am-dropdown-toggle tpl-header-list-link" href="javascript:;">
                        <span class="tpl-header-list-user-nick">欢迎<%=ss.getStuName().substring(0, 1) %>同学</span><span class="tpl-header-list-user-ico"> <img src="img/download.jpg"></span>
                    </a>
                    <ul class="am-dropdown-content">
                        <li><a href="login.jsp"><span class="am-icon-cog"></span>切换账号</a></li>
                        <li><a href="login.jsp"><span class="am-icon-power-off"></span> 退出</a></li>
                    </ul>
                </li>
				</ul>
        </div>
    </header>



<script type="text/javascript">
function kaosi(){
	$.ajax({
    	url: "QianTaiServlet?op=kaosi&bjid=${model.classId}",
		dataType: "text",
		type: "post",
		success:function(data){
		    location.href="kaosi.jsp";
		}
    });	
};
function cx(){
	if($("#cx").val()=="请选择试卷"){
		$("#cj").text("");
		$("#tishi").text("");
	}else{
		$.ajax({
	    	url: "QianTaiServlet",
			dataType: "text",
			type: "post",
			data:"op=cxcj&sjid="+$("#cx").val(),
		    success:function(data){
		    	if(data==""){
			    	$("#tishi").text("您可能未参加该场考试！");
		    	}
		    	else if(Number(data)<60){
		    		$("#tishi").text("太遗憾了！");
		    	}else if(Number(data)>=60){
		    		$("#tishi").text("恭喜你，通过了考试！");
		    	}
			    $("#cj").text(data);
			}
	    });
	}
}
</script>


    <div class="tpl-page-container tpl-page-header-fixed">


        <div class="tpl-left-nav tpl-left-nav-hover" style="margin-right: 20px;">
            <div class="tpl-left-nav-title">
                菜单列表
            </div>
            <div class="tpl-left-nav-list">
                <ul class="tpl-left-nav-menu">
                    <li class="tpl-left-nav-item">
                        <a href="indexs.jsp" class="nav-link active">
                            <i class="am-icon-home"></i>
                            <span>首页</span>
                        </a>
                    </li>
					
                    
                   
                    <li class="tpl-left-nav-item">
                        <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                            <i class="am-icon-wpforms"></i>
                            <span>考试中心</span>
                            <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right tpl-left-nav-more-ico-rotate"></i>
                        </a>
                        <ul class="tpl-left-nav-sub-menu" style="display: block;">
                            <li>
                                 <a onclick="kaosi()" href="#">
                                    <span>考试</span>
                                </a>

                                <a href="chaxuncj.jsp">
                                    <span>成绩查询</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                   
                    <li class="tpl-left-nav-item">
                        <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                            <i class="am-icon-table"></i>
                            <span>个人中心</span>
                        <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right tpl-left-nav-more-ico-rotate"></i>
                        </a>
                        <ul class="tpl-left-nav-sub-menu" style="display: block;">
                            <li>
                                <a href="xiugaimm.jsp">
                                    <span>修改密码</span>
                                </a>
                                <a href="gerengzx.jsp">
                                    <span>个人信息</span>
                                 </a>
                    
                            </li>
                        </ul>
                    </li>
					<li class="tpl-left-nav-item">
					    <a href="login.jsp" class="nav-link tpl-left-nav-link-list" style="background-color: gainsboro;">
					        <i class="am-icon-key"></i>
					        <span>退出登陆</span>
					
					    </a>
					</li>
                </ul>
            </div>
        </div>
		
		
		
					<%
						PublishExamPlanDao ped=new PublishExamPlanDao();
						ScoreInfoDao sid=new ScoreInfoDao();
						StudentInfo s=(StudentInfo)request.getSession().getAttribute("model");
						int bjid=s.getClassId();
						ArrayList<PublishExamPlan>list=ped.selectAllByBjid(bjid);
						
					%>
		成绩查询
		<div class="tpl-portlet-components" style="border-radius:8px;height: 555px;">
			<table align="center" style="margin-top: 10%;">
				<tr>
					<td>--------<img style="width: 30px;"></td>
					<td>请选择试卷</td>
					<td><img style="width: 30px;">--------</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr align="center">
					<td colspan="3">
								<select onchange="cx()" name="curname" id="cx" style="width:200px;height:34px;">
							  		<option>请选择试卷</option>
							  		<%for(PublishExamPlan p:list){ %>
							  			<option value="<%=p.getPaperId() %>"><%=p.getPaperName() %></option>
							  		<%} %>
								</select>
					</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr align="center">
					<td align="right">成绩：</td>
					<td align="center" style="border:1px solid dimgray;">
						<font id="cj">&nbsp;</font>
					</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr><tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr align="center">
					<td colspan="3" align="center">
						<font id="tishi" color="red"></font>
					</td>
				</tr>
			</table>
		</div>
		
    </div>
</body>

</html>