<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
  <script>
  	$(function(){
  		$('#fffcur').form({
     		success:function(data){
     			if(data=="注册成功"){
	     			$.messager.alert('温馨提示',data);
     			}else{
     				$.messager.alert('温馨提示',data);
     			}
     		}
     	});
  	});
  </script>

</head>
<body>
	<div align="center" style="margin-top:10%">
				<form id="fffcur" action="CurriInfoServlet?op=add" method="post">
					<table align="center" style="padding:10px;">
						<tr>
							<td>科目名称：</td>
							<td>
								<input name="namecur" data-options="required:true,validType:'length[2,10]'" class="easyui-textbox" prompt="请输入科目名称" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr align="center">
							<td colspan="2">
								<a class="easyui-linkbutton">
									<input type="submit" value="添加" style="background-color:transparent;border:0px;width:100px;height:30px;">
								</a>
							</td>
						</tr>
					</table>
				</form>		
			</div>
	<script>
	  	$(function(){
	  		$('#fffcur').form({
	     		success:function(data){
	     			if(data=="添加成功"){
		     			$.messager.alert('温馨提示',data);
	     			}else{
	     				$.messager.alert('温馨提示',data+"请检查后重试！");
	     			}
			    	$('#dgcur').datagrid('load');
	     		}
	     	});
	  		
	  	});
  </script>
</body>
</html>