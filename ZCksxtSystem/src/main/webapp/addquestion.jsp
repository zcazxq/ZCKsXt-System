<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.dao.*,java.util.*,com.model.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>

</head>
<body>
	<div alegin="center" style="margin-top:2%">
				<form id="quff" action="QuestionInfoServlet?op=add" method="post">
					<table align="center" style="padding:50px; float: left;margin-left: 8%">
						<tr>
							<% 
								CurriInfoDao cd=new CurriInfoDao();
								ArrayList<CurriInfo> list=cd.selectAll();
							%>
							<td>科目名称：</td>
							<td>
								<select name="curname" id="quccs" data-options="prompt:'请选择科目',editable:false," class="easyui-combobox" required="required" name="dept" style="width:300px;height:34px;">
							  		<option value="0"></option>
							  		<%for(CurriInfo l:list){ %>
							  		<option value="<%=l.getCurId() %>"><%=l.getCurName() %></option>
							  		<%} %>
								</select>	<br><br>
							</td>
						</tr>
						<tr>
							<td>题&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;目：</td>
							<td>
								<input name="timuname" data-options="required:true,multiline:true," class="easyui-textbox" prompt="请输入题目" data-options="iconCls:'icon-man'" style="width:300px;height:60px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
							<td>A：</td>
							<td>
								<input name="A" data-options="required:true" class="easyui-textbox" prompt="请输入答案A" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
							<td>B：</td>
							<td>
								<input name="B" data-options="required:true" class="easyui-textbox" prompt="请输入答案B" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
							<td>C：</td>
							<td>
								<input name="C" data-options="required:true" class="easyui-textbox" prompt="请输入答案C" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
							<td>D：</td>
							<td>
								<input name="D" data-options="required:true" class="easyui-textbox" prompt="请输入答案D" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						
					</table>
					
					<table align="center" style="padding:50px;float: left">
					<tr>
							<td>参考答案：</td>
							<td>
								<select name="yes" id="yes" class="easyui-combobox" data-options="prompt:'请选择参考答案',editable:false," required="required" style="width:300px;height:34px;">
							  		<option></option>
							  		<option>A</option>
							  		<option>B</option>
							  		<option>C</option>
							  		<option>D</option>
								</select>	<br><br>
							</td>
						</tr>
						<tr>
							<td>解&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;析：</td>
							<td>
								<input name="jiexi" data-options="required:true,multiline:true," class="easyui-textbox" prompt="请输入解析" data-options="iconCls:'icon-man'" style="width:300px;height:180px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
							<td>分&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;值：</td>
							<td>
								<input name="fenzhi" data-options="required:true" class="easyui-textbox" prompt="请输入分值" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr align="center">
							<td colspan="2">
								<a class="easyui-linkbutton">
									<input type="submit" value="录入" style="background-color:transparent;border:0px;width:100px;height:30px;">
								</a>
							</td>
						</tr>
					</table>
				</form>		
			</div>
	<script>
	  	$(function(){
	  		$('#quff').form({
	     		success:function(data){
	     			if(data=="添加成功"){
		     			$.messager.alert('温馨提示',data);
	     			}else{
	     				$.messager.alert('温馨提示',data+"请检查后重试！");
	     			}
			    	$('#qudgs').datagrid('load');
	     		}
	     	});
	  		
	  	});
  </script>
</body>
</html>