<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.dao.*,java.util.*,com.model.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>

</head>
<body>
	<div alegin="center" style="margin-top:5%">
				<form id="ff" action="StuServlet?op=add" method="post">
					<table align="center" style="padding:10px;">
						<tr>
							<td>性&nbsp; &nbsp;名：</td>
							<td>
								<input name="stuname" data-options="required:true,validType:'length[2,10]'" class="easyui-textbox" prompt="请输入姓名" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
							<td>性&nbsp; &nbsp;别：</td>
							<td>
								<input class="easyui-radiobutton" name="stusex" value="男" data-options="checked:true">男
								<input class="easyui-radiobutton" name="stusex" value="女">女<br><br>
							</td>
						</tr>
						<tr>
							<td>密&nbsp;&nbsp;码：</td>
							<td>
								<input name="stupwd" data-options="required:true,validType:'length[2,10]'" class="easyui-textbox" prompt="请输入密码" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
							<% 
								ClassDao cl=new ClassDao();
								ArrayList<com.model.Class> list=cl.selectAll();
							%>
							<td>班级名称：</td>
							<td>
								<select name="classname" id="ccssss" data-options="editable:false,prompt:'请选择班级'," class="easyui-combobox" required="required" name="dept" style="width:300px;height:34px;"><br><br>
							  		<option value="0"></option>
							  		<%for(com.model.Class l:list){ %>
							  		<option value="<%=l.getClassId() %>"><%=l.getClassName() %></option>
							  		<%} %>
								</select>	<br><br>
							</td>
						</tr>
						<tr>
							<td>电话号码：</td>
							<td>
								<input name="stuphone" data-options="required:true,validType:'length[11,11]'" class="easyui-textbox" prompt="请输入联系电话" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
							<td>身份证号：</td>
							<td>
								<input name="stustatus" data-options="required:true,validType:'length[18,19]'" class="easyui-textbox" prompt="请输入身份证号" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr align="center">
							<td colspan="2">
								<a class="easyui-linkbutton">
									<input type="submit" value="录入" style="background-color:transparent;border:0px;width:100px;height:30px;">
								</a>
							</td>
						</tr>
					</table>
				</form>		
			</div>
	<script>
	  	$(function(){
	  		$('#ff').form({
	     		success:function(data){
	     			if(data=="添加成功"){
		     			$.messager.alert('温馨提示',data);
	     			}else{
	     				$.messager.alert('温馨提示',data+"请检查后重试！");
	     			}
			    	$('#dgs').datagrid('load');
	     		}
	     	});
	  		
	  	});
  </script>
</body>
</html>