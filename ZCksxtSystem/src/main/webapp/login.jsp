<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>在线考试平台</title>
 <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <link rel="stylesheet" href="css/message.min.css">
 <link rel="stylesheet" href="css/hsycmsAlert.min.css">
    <link rel="icon" type="image/png" href="assets/i/favicon.png">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="js/message.min.js"></script>
 <script src="js/hsycmsAlert.min.js"></script>
<style>
:root {
	/* COLORS */
	--gray: #333;
	--blue: #0367a6;
	--lightblue: #008997;

	/* RADII */
	--button-radius: 0.7rem;

	/* SIZES */
	--max-width: 758px;
	--max-height: 420px;

	font-size: 16px;
	font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen,
		Ubuntu, Cantarell, "Open Sans", "Helvetica Neue", sans-serif;
}

body {
	align-items: center;
	background-color: var(--white);
	background: url("img/yt.jpg");
	background-attachment: fixed;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
	display: grid;
	height: 100vh;
	place-items: center;
}

.form__title {
	color: snow;
	font-weight: 300;
	margin: 0;
	margin-bottom: 1.25rem;
}

.link {
	color: var(--gray);
	font-size: 0.9rem;
	margin: 1.5rem 0;
	text-decoration: none;
}

.container {
	background-color: var(--white);
	border-radius: var(--button-radius);
	box-shadow: 0 0.9rem 1.7rem rgba(0, 0, 0, 0.25),
		0 0.7rem 0.7rem rgba(0, 0, 0, 0.22);
	height: var(--max-height);
	max-width: var(--max-width);
	overflow: hidden;
	position: relative;
	width: 100%;
}

.container__form {
	height: 100%;
	position: absolute;
	top: 0;
	transition: all 0.6s ease-in-out;
}

.container--signin {
	left: 0;
	width: 50%;
	z-index: 2;
}

.container.right-panel-active .container--signin {
	transform: translateX(100%);
}

.container--signup {
	left: 0;
	opacity: 0;
	width: 50%;
	z-index: 1;
}

.container.right-panel-active .container--signup {
	animation: show 0.6s;
	opacity: 1;
	transform: translateX(-100%);
	z-index: 5;
}

.container__overlay {
	height: 100%;
	left: 50%;
	overflow: hidden;
	position: absolute;
	top: 0;
	transition: transform 0.6s ease-in-out;
	width: 50%;
	z-index: 100;
}

.container.right-panel-active .container__overlay {
	transform: translateX(-100%);
}

.overlay {
	background-color: var(--lightblue);
	background: url("img/loginbj.png");
	/*低版本有问题
	 background-attachment: fixed; 
	background-position: center;*/
	background-repeat: no-repeat;
	background-size: cover;
	height: 100%;
	left: -100%;
	position: relative;
	transform: translateX(0);
	transition: transform 0.6s ease-in-out;
	width: 200%;
}

.container.right-panel-active .overlay {
	transform: translateX(50%);
}

.overlay__panel {
	align-items: center;
	display: flex;
	flex-direction: column;
	height: 100%;
	justify-content: center;
	position: absolute;
	text-align: center;
	top: 0;
	transform: translateX(0);
	transition: transform 0.6s ease-in-out;
	width: 50%;
}

.overlay--left {
	transform: translateX(-20%);
}

.container.right-panel-active .overlay--left {
	transform: translateX(0);
}

.overlay--right {
	right: 0;
	transform: translateX(0);
}

.container.right-panel-active .overlay--right {
	transform: translateX(20%);
}

.btn {
	background-color: var(--blue);
	background-image: linear-gradient(90deg, var(--blue) 0%, var(--lightblue) 74%);
	border-radius: 20px;
	border: 1px solid var(--blue);
	color: white;
	cursor: pointer;
	font-size: 0.8rem;
	font-weight: bold;
	letter-spacing: 0.1rem;
	padding: 0.9rem 4rem;
	text-transform: uppercase;
	transition: transform 80ms ease-in;
}

.form > .btn {
	/* margin-top: 1.5rem; */
}

.btn:active {
	transform: scale(0.95);
}

.btn:focus {
	outline: none;
}

.form {
	display: flex;
	align-items: center;
	justify-content: center;
	flex-direction: column;
	padding: 0 3rem;
	height: 100%;
	text-align: center;
}

.input {
	border: none;
	padding: 0.9rem 0.9rem;
	/* margin: 0.3rem 0; */
	width: 100%;
}

@keyframes show {
	0%,
	49.99% {
		opacity: 0;
		z-index: 1;
	}

	50%,
	100% {
		opacity: 1;
		z-index: 5;
	}
}
</style>
<style>

.container {
  position: absolute;
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  top: 50%;
  left: 50%;
}

form {
  background: rgba(255, 255, 255, 0.3);
  padding: 3em;
  height: 320px;
  border-radius: 20px;
  border-left: 1px solid rgba(255, 255, 255, 0.3);
  border-top: 1px solid rgba(255, 255, 255, 0.3);
  -webkit-backdrop-filter: blur(10px);
          backdrop-filter: blur(10px);
  box-shadow: 20px 20px 40px -6px rgba(0, 0, 0, 0.2);
  text-align: center;
  position: relative;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}
form p {
  font-weight: 500;
  color: snow;
  opacity: 0.7;
  font-size: 1.4rem;
  margin-top: 0;
  margin-bottom: 60px;
  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);
}
form a {
  text-decoration: none;
  color: #ddd;
  font-size: 12px;
}
form a:hover {
  text-shadow: 2px 2px 6px #00000040;
}
form a:active {
  text-shadow: none;
}
form input {
  background: transparent;
  width: 200px;
  padding: 1em;
  margin-bottom: 2em;
  border: none;
  border-left: 1px solid rgba(255, 255, 255, 0.3);
  border-top: 1px solid rgba(255, 255, 255, 0.3);
  border-radius: 5000px;
  -webkit-backdrop-filter: blur(5px);
          backdrop-filter: blur(5px);
  box-shadow: 4px 4px 60px rgba(0, 0, 0, 0.2);
  color: snow;
  font-family: Montserrat, sans-serif;
  font-weight: 500;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}
form input:hover {
  background: rgba(255, 255, 255, 0.1);
  box-shadow: 4px 4px 60px 8px rgba(0, 0, 0, 0.2);
}
form input[type="email"]:focus, form input[type="password"]:focus {
	background-image: url("1002-1928x1080.jpg");
  background: rgba(255, 255, 255, 0.1);
  box-shadow: 4px 4px 60px 8px rgba(0, 0, 0, 0.2);
}
form input[type="button"] {
  margin-top: 10px;
  width: 150px;
  font-size: 1rem;
}
form input[type="button"]:hover {
  cursor: pointer;
}
form input[type="button"]:active {
  background: rgba(255, 255, 255, 0.2);
}

::-moz-placeholder {
  font-family: Montserrat, sans-serif;
  font-weight: 400;
  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.4);
}



::placeholder {
  color: snow;
}

.drop {
  background: rgba(255, 255, 255, 0.3);
  -webkit-backdrop-filter: blur(10px);
          backdrop-filter: blur(10px);
  border-radius: 10px;
  border-left: 1px solid rgba(255, 255, 255, 0.3);
  border-top: 1px solid rgba(255, 255, 255, 0.3);
  box-shadow: 10px 10px 60px -8px rgba(0, 0, 0, 0.2);
  position: absolute;
  -webkit-transition: all 0.2s ease;
  transition: all 0.2s ease;
}
.drop-1 {
  height: 80px;
  width: 80px;
  top: -20px;
  left: -40px;
  z-index: -1;
}
.drop-2 {
  height: 80px;
  width: 80px;
  bottom: -30px;
  right: -10px;
}
.drop-3 {
  height: 100px;
  width: 100px;
  bottom: 120px;
  right: -50px;
  z-index: -1;
}
.drop-4 {
  height: 120px;
  width: 120px;
  top: -60px;
  right: -60px;
}
.drop-5 {
  height: 60px;
  width: 60px;
  bottom: 170px;
  left: 90px;
  z-index: -1;
}

a,
input:focus,
select:focus,
textarea:focus,
button:focus {
  outline: none;
}

</style>
<!-- 验证码 -->
<style type="text/css">
        #canvas{
            float:right;
            display: inline-block;
            border:1px solid #ccc;
            border-radius: 5px;
            cursor: pointer;
        }
</style>
</head>
<body style="overflow:-Scroll;overflow-y:hidden">
<div class="container right-panel-active">
    <!-- Sign Up -->
    <!-- <div class="container__form container--signup">
       <form action="#" class="form" id="form1">
            <h2 class="form__title">登陆</h2>
            <input type="text" placeholder="User" class="input" />
            <input type="email" placeholder="Email" class="input" />
            <input type="password" placeholder="Password" class="input" />
            <button class="btn">登陆</button>
        </form>
    </div> -->

    <!-- Sign In -->
    <div class="container__form container--signin">
        <form class="form" id="form2">
            <h2 class="form__title">登&nbsp;&nbsp;&nbsp;&nbsp;陆</h2>
            
            <input id="txtname" type="text" placeholder="账号" class="input" />
            <input id="pass" type="password" placeholder="密码" class="input" />
            <table>
            	<tr>
            		<td style="position: relative;">
            		<span style="height:120px">&nbsp;</span>
            			<input style="position:absolute;left: -130px;width: 165px;" type="text" placeholder="验证码（不区分大小写）" class="input-val" style="width:200px;height:50px;"/>
            		</td>
            		<td style="position: relative;">
            			<canvas style="position:absolute;top: 3px;left: 35px;height: 35px;"  id="canvas" width="95" height="35"></canvas>
           			</td>
            	</tr>
            </table>
            <a href="#" style="color: skyblue;margin-top: 30px;margin-left: 200px;" class="link">忘记密码?</a><input type="hidden" id="js" value="0">
            <div id="btns" class="btn" >登&nbsp;&nbsp;陆</div>
        </form>
    </div>

    <!-- Overlay -->
    <div class="container__overlay">
        <div class="overlay">
            <div class="overlay__panel overlay--left">
                <button class="btn" id="signIn">管&nbsp;&nbsp;理&nbsp;&nbsp;员</button>
            </div>
            <div class="overlay__panel overlay--right">
                <button class="btn" id="signUp">学&nbsp;&nbsp;&nbsp;&nbsp;生</button>
            </div>
        </div>
    </div>
</div>
<!-- 消息框 -->
<script>
	const signInBtn = document.getElementById("signIn");
	const signUpBtn = document.getElementById("signUp");
	const fistForm = document.getElementById("form1");
	const secondForm = document.getElementById("form2");
	const container = document.querySelector(".container");
	
	signInBtn.addEventListener("click", () => {
		$("#js").val("1");
		container.classList.remove("right-panel-active");
	});
	
	signUpBtn.addEventListener("click", () => {
		$("#js").val("0");
		container.classList.add("right-panel-active");
	});
	  $(function(){
		  var system ={};  
          var p = navigator.platform;       
          system.win = p.indexOf("Win") == 0;  
          system.mac = p.indexOf("Mac") == 0;  
          system.x11 = (p == "X11") || (p.indexOf("Linux") == 0);     
          if(system.win||system.mac||system.xll){//如果是电脑跳转到
              
          }else{  //如果是手机,跳转到
          	location.href="404.jsp";
          }
	        var show_num = [];
	        draw(show_num);

	        $("#canvas").on('click',function(){
	            draw(show_num);
	        })
	        $("#btns").click(function(){
	        	if($("#txtname").val()==""||$("#pass").val()==""){
	        		hsycms.tips('信息不完整！',()=>{
     			 	},1500);
	        	}else{
		            var val = $(".input-val").val().toLowerCase();
		            var num = show_num.join("");
		            if(val==''){
		            	hsycms.tips('请输入验证码！',()=>{
	     			 	},1500);
		            	
		            }else if(val == num){
		            	  /* hsycms.loading('正在加载');
		                //2秒后隐藏
		               setTimeout(res=>{
    				    	hsycms.closeAll();
		                },5000); */ 
		            	$.ajax
		    		    ({
		    		    	url: "UserInfoStuLoginServlet",
		    				dataType: "text",
		    				type: "post",
		    				data:"op="+$("#js").val()+"&name="+$("#txtname").val()+"&pwd="+$("#pass").val(),
		    			    success:function(data){
		    				   if(data=="登陆成功"){
		    		                $(".input-val").val('');
		    		                draw(show_num);
		    		                location.href="index.jsp";
		    				    }else if(data!="账号或密码有误！"){
		    				    	$(".input-val").val('');
		    		                draw(show_num);
		    		                $.ajax({
		    		    		    	url: "QianTaiServlet?op=kaosi&bjid="+data,
		    		    				dataType: "text",
		    		    				type: "post",
		    		    				success:function(data){
		    		    					location.href="indexs.jsp";
		    		    				}
		    		    		    });	
		    				    }else{
		    				    	hsycms.tips("账号密码有误！",()=>{
		    				    		$(".input-val").val('');
			    		                draw(show_num);
		    	     			 	},1500);
		    				    } 
		    				}
		    		    });
		            }else{
		            	hsycms.tips('验证码有误！',()=>{
	     			 	},1500);
		                $(".input-val").val('');
		                draw(show_num);
		            }
	        	}
	        });
	    });
</script>
<!-- 验证码 -->
<script>
    function draw(show_num) {
        var canvas_width=$('#canvas').width();
        var canvas_height=$('#canvas').height();
        var canvas = document.getElementById("canvas");//获取到canvas的对象，演员
        var context = canvas.getContext("2d");//获取到canvas画图的环境，演员表演的舞台
        canvas.width = canvas_width;
        canvas.height = canvas_height;
        var sCode = "A,B,C,E,F,G,H,J,K,L,M,N,P,Q,R,S,T,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0";
        var aCode = sCode.split(",");
        var aLength = aCode.length;//获取到数组的长度
        
        for (var i = 0; i <= 3; i++) {
            var j = Math.floor(Math.random() * aLength);//获取到随机的索引值
            var deg = Math.random() * 30 * Math.PI / 180;//产生0~30之间的随机弧度
            var txt = aCode[j];//得到随机的一个内容
            show_num[i] = txt.toLowerCase();
            var x = 10 + i * 20;//文字在canvas上的x坐标
            var y = 20 + Math.random() * 8;//文字在canvas上的y坐标
            context.font = "bold 23px 微软雅黑";

            context.translate(x, y);
            context.rotate(deg);

            context.fillStyle = randomColor();
            context.fillText(txt, 0, 0);

            context.rotate(-deg);
            context.translate(-x, -y);
        }
        for (var i = 0; i <= 5; i++) { //验证码上显示线条
            context.strokeStyle = randomColor();
            context.beginPath();
            context.moveTo(Math.random() * canvas_width, Math.random() * canvas_height);
            context.lineTo(Math.random() * canvas_width, Math.random() * canvas_height);
            context.stroke();
        }
        for (var i = 0; i <= 30; i++) { //验证码上显示小点
            context.strokeStyle = randomColor();
            context.beginPath();
            var x = Math.random() * canvas_width;
            var y = Math.random() * canvas_height;
            context.moveTo(x, y);
            context.lineTo(x + 1, y + 1);
            context.stroke();
        }
    }

    function randomColor() {//得到随机的颜色值
        var r = Math.floor(Math.random() * 256);
        var g = Math.floor(Math.random() * 256);
        var b = Math.floor(Math.random() * 256);
        return "rgb(" + r + "," + g + "," + b + ")";
    }
</script>
</body>
</html>
