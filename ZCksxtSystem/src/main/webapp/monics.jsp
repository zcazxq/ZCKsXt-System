<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.model.*"%>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>在线考试平台</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="assets/css/admin.css">
    <link rel="stylesheet" href="assets/css/app.css">
    <script src="assets/js/echarts.min.js"></script>
    <script type="text/javascript">
    function kaosi(){
		$.ajax({
	    	url: "QianTaiServlet?op=kaosi&bjid=${model.classId}",
			dataType: "text",
			type: "post",
			success:function(data){
			    location.href="kaosi.jsp";
			}
	    });	
	};
    </script>
</head>
<%
	StudentInfo ss=(StudentInfo)request.getSession().getAttribute("model");
%>
<body data-type="index" style="overflow: hidden;">


    <header class="am-topbar am-topbar-inverse admin-header">
        <div class="am-topbar-brand">
            <a href="javascript:;" class="tpl-logo">
                <img src="assets/img/logo.png" alt="">
            </a>
        </div>

        <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>

        <div class="am-collapse am-topbar-collapse" id="topbar-collapse">

            <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list tpl-header-list">
                
                <li class="am-dropdown" data-am-dropdown data-am-dropdown-toggle>
                     <a class="am-dropdown-toggle tpl-header-list-link" href="javascript:;">
                        <span class="tpl-header-list-user-nick">欢迎<%=ss.getStuName().substring(0, 1) %>同学</span><span class="tpl-header-list-user-ico"> <img src="assets/img/user01.png"></span>
                    </a>
                    <ul class="am-dropdown-content">
                        <li><a href="#"><span class="am-icon-bell-o"></span> 资料</a></li>
                        <li><a href="#"><span class="am-icon-cog"></span> 设置</a></li>
                        <li><a href="#"><span class="am-icon-power-off"></span> 退出</a></li>
                    </ul>
                </li>
                <li><a href="###" class="tpl-header-list-link"><span class="am-icon-sign-out tpl-header-list-ico-out-size"></span></a></li>
            </ul>
        </div>
    </header>







    <div class="tpl-page-container tpl-page-header-fixed">


        <div class="tpl-left-nav tpl-left-nav-hover" style="margin-right: 20px;">
            <div class="tpl-left-nav-title">
                菜单列表
            </div>
            <div class="tpl-left-nav-list">
                <ul class="tpl-left-nav-menu">
                    <li class="tpl-left-nav-item">
                        <a href="indexs.jsp" class="nav-link active">
                            <i class="am-icon-home"></i>
                            <span>首页</span>
                        </a>
                    </li>
					
                    
                    
                    <li class="tpl-left-nav-item">
                        <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                            <i class="am-icon-wpforms"></i>
                            <span>考试中心</span>
                            <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right tpl-left-nav-more-ico-rotate"></i>
                        </a>
                        <ul class="tpl-left-nav-sub-menu" style="display: block;">
                            <li>
                                 <a onclick="kaosi()" href="#">
                                    <span>考试</span>
                                </a>

                                <a href="chaxuncj.jsp">
                                    <span>成绩查询</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                   
                    <li class="tpl-left-nav-item">
                        <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                            <i class="am-icon-table"></i>
                            <span>练习中心</span>
                        <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right tpl-left-nav-more-ico-rotate"></i>
                        </a>
                        <ul class="tpl-left-nav-sub-menu" style="display: block;">
                            <li>
                                <a href="shijilx.jsp">
                                    <span>随机练习</span>
                                 </a>

                                <a href="monics.jsp">
                                    <span>模拟测试</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="tpl-left-nav-item">
                        <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                            <i class="am-icon-table"></i>
                            <span>个人中心</span>
                        <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right tpl-left-nav-more-ico-rotate"></i>
                        </a>
                        <ul class="tpl-left-nav-sub-menu" style="display: block;">
                            <li>
                                <a href="xiugaimm.jsp">
                                    <span>修改密码</span>
                                </a>
                                <a href="gerengzx.jsp">
                                    <span>个人信息</span>
                                 </a>
                    
                            </li>
                        </ul>
                    </li>
					<li class="tpl-left-nav-item">
					    <a href="login.jsp" class="nav-link tpl-left-nav-link-list" style="background-color: gainsboro;">
					        <i class="am-icon-key"></i>
					        <span>退出登陆</span>
					
					    </a>
					</li>
                </ul>
            </div>
        </div>
		
		
		
		模拟测试
		<div class="tpl-portlet-components" style="border-radius:8px;height: 555px;">
			<ul>
				<li style="">
					<div style="border-radius:8px;padding: 10px;border: 1px solid silver;width: 940px;height: 100px;float: left;">
						<div style="float: left;">
							<img style="border: 1px solid salmon;width: 80px;height: 80px;"/>
					
						</div>
						<div style="margin-left: 5px; float: left; border: 0px solid red;width: 830px;height: 80px;">
							<table>
								<tr>
									<td colspan="3">&nbsp;&nbsp;试卷名称</td>
									<td rowspan="2" align="right" style="width: 400px;"><br />
										<div style="width: 300px;">
											2023-5-1 12:12:12<img style="width: 30px;">
											<a href="#">开始测试</a>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp;考试时长：150分钟</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;共计：50道</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;总分：100分</td>
								</tr>
							</table>
						</div>
					</div>
				</li>
				<li>
					<div style="width:10px;">&nbsp;</div>
				</li>
				<li>
					<div style="border-radius:8px;padding: 10px;border: 1px solid silver;width: 940px;height: 100px;float: left;">
						<div style="float: left;">
							<img style="border: 1px solid salmon;width: 80px;height: 80px;"/>
					
						</div>
						<div style="margin-left: 5px; float: left; border: 0px solid red;width: 830px;height: 80px;">
							<table>
								<tr>
									<td colspan="3">&nbsp;&nbsp;试卷名称</td>
									<td rowspan="2" align="right" style="width: 400px;"><br />
										<div style="width: 300px;">
											2023-5-1 12:12:12<img style="width: 30px;">
											<a href="#">开始测试</a>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp;考试时长：150分钟</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;共计：50道</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;总分：100分</td>
								</tr>
							</table>
						</div>
					</div>
				</li>
			</ul>
			
		</div>
		
    </div>


    <script src="http://www.jq22.com/jquery/jquery-2.1.1.js"></script>
    <script src="assets/js/amazeui.min.js"></script>
    <script src="assets/js/iscroll.js"></script>
    <script src="assets/js/app.js"></script>
</body>

</html>