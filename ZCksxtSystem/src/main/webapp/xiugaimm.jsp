<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.model.*"%>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>在线考试平台</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="assets/css/admin.css">
    <link rel="stylesheet" href="assets/css/app.css">
     <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
     <link rel="stylesheet" href="css/hsycmsAlert.min.css">
    <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
	<script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
	<script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
    <script src="assets/js/echarts.min.js"></script>
 	<script src="js/hsycmsAlert.min.js"></script>
    
	<style type="text/css">
        #canvas{
            float:right;
            display: inline-block;
            border:1px solid #ccc;
            border-radius: 5px;
            cursor: pointer;
        }
    </style>
    <script>
    function kaosi(){
		$.ajax({
	    	url: "QianTaiServlet?op=kaosi&bjid=${model.classId}",
			dataType: "text",
			type: "post",
			success:function(data){
			    location.href="kaosi.jsp";
			}
	    });	
	};
		    $(function(){
		        var show_num = [];
		        draw(show_num);
		
		        $("#canvas").on('click',function(){
		            draw(show_num);
		        })
		        $("#btn").on('click',function(){
		        	
		            var val = $(".input-val").val().toLowerCase();
		            var num = show_num.join("");
		            if(val==''){
		                hsycms.tips("请输入验证码！",()=>{
		                    console.log("提示关闭后");
		                  },1500);
		            }else if(val == num){
		            	if($("#pwd").val()==""||$("#qrpwd").val()==""){
		            		hsycms.tips("请输入密码！",()=>{
			                    console.log("提示关闭后");
			                  },1500)
		            	}
		            	else if($("#pwd").val()==$("#qrpwd").val()){
		            		$.ajax
			    		    ({
			    		    	url: "StuServlet",
			    				dataType: "text",
			    				type: "post",
			    				data:"op=xiugai&pwd="+$("#qrpwd").val(),
			    			    success:function(data){
			    			    	hsycms.tips(data,()=>{
					                    console.log("提示关闭后");
					                  },1500)
			    				}
			    		    });
		            		
		            	}
		            	else{
		            		hsycms.tips("两次密码不一致！",()=>{
			                    console.log("提示关闭后");
			                  },1500)
		            	}
		                draw(show_num);
		
		            }else{
		            	hsycms.tips("验证码错误！",()=>{
		                    console.log("提示关闭后");
		                  },1500);
		                draw(show_num);
		            }
		        })
		    })
		
		    function draw(show_num) {
		        var canvas_width=$('#canvas').width();
		        var canvas_height=$('#canvas').height();
		        var canvas = document.getElementById("canvas");//获取到canvas的对象，演员
		        var context = canvas.getContext("2d");//获取到canvas画图的环境，演员表演的舞台
		        canvas.width = canvas_width;
		        canvas.height = canvas_height;
		        var sCode = "A,B,C,E,F,G,H,J,K,L,M,N,P,Q,R,S,T,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0";
		        var aCode = sCode.split(",");
		        var aLength = aCode.length;//获取到数组的长度
		        
		        for (var i = 0; i <= 3; i++) {
		            var j = Math.floor(Math.random() * aLength);//获取到随机的索引值
		            var deg = Math.random() * 30 * Math.PI / 180;//产生0~30之间的随机弧度
		            var txt = aCode[j];//得到随机的一个内容
		            show_num[i] = txt.toLowerCase();
		            var x = 10 + i * 20;//文字在canvas上的x坐标
		            var y = 20 + Math.random() * 8;//文字在canvas上的y坐标
		            context.font = "bold 23px 微软雅黑";
		
		            context.translate(x, y);
		            context.rotate(deg);
		
		            context.fillStyle = randomColor();
		            context.fillText(txt, 0, 0);
		
		            context.rotate(-deg);
		            context.translate(-x, -y);
		        }
		        for (var i = 0; i <= 5; i++) { //验证码上显示线条
		            context.strokeStyle = randomColor();
		            context.beginPath();
		            context.moveTo(Math.random() * canvas_width, Math.random() * canvas_height);
		            context.lineTo(Math.random() * canvas_width, Math.random() * canvas_height);
		            context.stroke();
		        }
		        for (var i = 0; i <= 30; i++) { //验证码上显示小点
		            context.strokeStyle = randomColor();
		            context.beginPath();
		            var x = Math.random() * canvas_width;
		            var y = Math.random() * canvas_height;
		            context.moveTo(x, y);
		            context.lineTo(x + 1, y + 1);
		            context.stroke();
		        }
		    }
		
		    function randomColor() {//得到随机的颜色值
		        var r = Math.floor(Math.random() * 256);
		        var g = Math.floor(Math.random() * 256);
		        var b = Math.floor(Math.random() * 256);
		        return "rgb(" + r + "," + g + "," + b + ")";
		    }
		</script>
</head>
<%
	StudentInfo ss=(StudentInfo)request.getSession().getAttribute("model");
%>
<body data-type="index" style="overflow: hidden;">


    <header class="am-topbar am-topbar-inverse admin-header">
        <div class="am-topbar-brand">
            <a href="javascript:;" class="tpl-logo">
                <img src="assets/img/logo.png" alt="">
            </a>
        </div>

        <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>

        <div class="am-collapse am-topbar-collapse" id="topbar-collapse">

            <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list tpl-header-list">
                
                <li class="am-dropdown" data-am-dropdown data-am-dropdown-toggle>
                    <a class="am-dropdown-toggle tpl-header-list-link" href="javascript:;">
                        <span class="tpl-header-list-user-nick">欢迎<%=ss.getStuName().substring(0, 1) %>同学</span><span class="tpl-header-list-user-ico"> <img src="img/download.jpg"></span>
                    </a>
                    <ul class="am-dropdown-content">
                        <li><a href="login.jsp"><span class="am-icon-cog"></span>切换账号</a></li>
                        <li><a href="login.jsp"><span class="am-icon-power-off"></span> 退出</a></li>
                    </ul>
                </li>
				</ul>
        </div>
    </header>







    <div class="tpl-page-container tpl-page-header-fixed">


        <div class="tpl-left-nav tpl-left-nav-hover" style="margin-right: 20px;">
            <div class="tpl-left-nav-title">
                菜单列表
            </div>
            <div class="tpl-left-nav-list">
                <ul class="tpl-left-nav-menu">
                    <li class="tpl-left-nav-item">
                        <a href="indexs.jsp" class="nav-link active">
                            <i class="am-icon-home"></i>
                            <span>首页</span>
                        </a>
                    </li>
					

                  
                    <li class="tpl-left-nav-item">
                        <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                            <i class="am-icon-wpforms"></i>
                            <span>考试中心</span>
                            <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right tpl-left-nav-more-ico-rotate"></i>
                        </a>
                        <ul class="tpl-left-nav-sub-menu" style="display: block;">
                            <li>
                                 <a onclick="kaosi()" href="#">
                                    <span>考试</span>
                                </a>

                                <a href="chaxuncj.jsp">
                                    <span>成绩查询</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    
                    <li class="tpl-left-nav-item">
                        <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                            <i class="am-icon-table"></i>
                            <span>个人中心</span>
                        <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right tpl-left-nav-more-ico-rotate"></i>
                        </a>
                        <ul class="tpl-left-nav-sub-menu" style="display: block;">
                            <li>
                                <a href="xiugaimm.jsp">
                                    <span>修改密码</span>
                                </a>
                                <a href="gerengzx.jsp">
                                    <span>个人信息</span>
                                 </a>
                    
                            </li>
                        </ul>
                    </li>
					<li class="tpl-left-nav-item">
					    <a href="login.jsp" class="nav-link tpl-left-nav-link-list" style="background-color: gainsboro;">
					        <i class="am-icon-key"></i>
					        <span>退出登陆</span>
					
					    </a>
					</li>
                </ul>
            </div>
        </div>
		
		
		
		修改密码
		<div class="tpl-portlet-components" style="border-radius:8px;height: 555px;">
			<form  method="post">
					<table align="center" style="margin-top:10%; padding:10px;">
						<tr>
							<td>账号：</td>
							<td>
								<input id="stuname" value="${model.stuPhone }" name="stuphone" data-options="required:true,disabled:true" class="easyui-textbox" prompt="请输入联系电话" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
		            		<td>验证码：</td>
		            		<td>
		            			<input id="stuname" name="stuphone" data-options="required:true" class="easyui-textbox input-val" prompt="请输入验证码" data-options="iconCls:'icon-man'" style="width:200px;height:34px;padding:10px">
		            			<canvas style="height: 35px;"  id="canvas" width="95" height="35"></canvas><br><br>
		           			</td>
		            	</tr>
						<tr>
							<td>密&nbsp;&nbsp;码：</td>
							<td>
								<input id="pwd" name="stupwd" data-options="required:true,validType:'length[2,10]'" class="easyui-textbox" prompt="请输入密码" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
							<td>确认密码：</td>
							<td>
								<input id="qrpwd" name="stupwd" data-options="required:true,validType:'length[2,10]'" class="easyui-textbox" prompt="请输入确认密码" data-options="iconCls:'icon-man'" style="width:300px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr align="center">
							<td colspan="2">
								<a class="easyui-linkbutton">
									<input id="btn" type="button" value="修改" style="background-color:transparent;border:0px;width:100px;height:30px;">
								</a>
							</td>
						</tr>
					</table>
				</form>		
		</div>
		
    </div>

    <script src="http://www.jq22.com/jquery/jquery-2.1.1.js"></script>
    <script src="assets/js/amazeui.min.js"></script>
    <script src="assets/js/iscroll.js"></script>
    <script src="assets/js/app.js"></script>
    
</body>

</html>