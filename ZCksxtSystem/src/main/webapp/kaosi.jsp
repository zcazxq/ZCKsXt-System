<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.model.*,com.dao.*,java.util.*"%>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>在线考试平台</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="assets/css/admin.css">
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="css/hsycmsAlert.min.css">
 	<script src="js/hsycmsAlert.min.js"></script>
    <script src="assets/js/echarts.min.js"></script>
    
<%
	StudentInfo s=(StudentInfo)request.getSession().getAttribute("model");
%>
<style>
td{
	border:0px solid red;
}
</style>
    <script type="text/javascript">
    	
    	function kaosi(){
    		$.ajax({
		    	url: "QianTaiServlet?op=kaosi&bjid=${model.classId}",
				dataType: "text",
				type: "post",
				success:function(data){
				    location.href="kaosi.jsp";
				}
		    });	
    	};
    </script>
</head>
<body data-type="index">


    <header class="am-topbar am-topbar-inverse admin-header">
        <div class="am-topbar-brand">
            <a href="javascript:;" class="tpl-logo">
                <img src="assets/img/logo.png" alt="">
            </a>
        </div>

        <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>

        <div class="am-collapse am-topbar-collapse" id="topbar-collapse">

            <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list tpl-header-list">
                
                <li class="am-dropdown" data-am-dropdown data-am-dropdown-toggle>
                    <a class="am-dropdown-toggle tpl-header-list-link" href="javascript:;">
                        <span class="tpl-header-list-user-nick">欢迎<%=s.getStuName().substring(0, 1) %>同学</span><span class="tpl-header-list-user-ico"> <img src="img/download.jpg"></span>
                    </a>
                    <ul class="am-dropdown-content">
                        <li><a href="login.jsp"><span class="am-icon-cog"></span>切换账号</a></li>
                        <li><a href="login.jsp"><span class="am-icon-power-off"></span> 退出</a></li>
                    </ul>
                </li>
				</ul>
        </div>
    </header>





<script type="text/javascript">

function ksks(a){
	var start=new Date($(a).attr("start")).getTime();
	var end=new Date($(a).attr("end")).getTime();
	var date=new Date().getTime();
	var startonefive=new Date($(a).attr("start")).getTime()+(15*60000);
	if(date<start){
		 hsycms.tips("考试时间还未到！",()=>{
             console.log("提示关闭后");
           },1500);
	}else if(date>=startonefive&&date<end){
		hsycms.tips("考试时间已过15分钟，不能进入考试！",()=>{
            
          },1500);
		/* alert("考试时间已过15分钟，不能进入考试！"); */
	}else if(date>end){
		hsycms.tips("考试已经结束！",()=>{
            
          },1500);
		/* alert("考试已经结束！"); */
	}else{
		$.ajax({
	    	url: "QianTaiServlet?op=kaishiks&sjname="+$(a).attr("my"),
			dataType: "text",
			type: "post",
			success:function(data){
				if(data=="success"){
			   		 location.href="examination.jsp";
				}else{
					alert(data);
				}
			}
	    });
	}
	
};
</script>

    <div class="tpl-page-container tpl-page-header-fixed">


        <div class="tpl-left-nav tpl-left-nav-hover" style="margin-right: 20px;">
            <div class="tpl-left-nav-title">
                菜单列表
            </div>
            <div class="tpl-left-nav-list">
                <ul class="tpl-left-nav-menu">
                    <li class="tpl-left-nav-item">
                        <a href="indexs.jsp" class="nav-link active">
                            <i class="am-icon-home"></i>
                            <span>首页</span>
                        </a>
                    </li>
					
                    
                    <li class="tpl-left-nav-item">
                        <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                            <i class="am-icon-wpforms"></i>
                            <span>考试中心</span>
                            <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right tpl-left-nav-more-ico-rotate"></i>
                        </a>
                        <ul class="tpl-left-nav-sub-menu" style="display: block;">
                            <li>
                                 <a onclick="kaosi()" href="#">
                                    <span>考试</span>
                                </a>

                                <a href="chaxuncj.jsp">
                                    <span>成绩查询</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    
                    <li class="tpl-left-nav-item">
                        <a href="javascript:;" class="nav-link tpl-left-nav-link-list">
                            <i class="am-icon-table"></i>
                            <span>个人中心</span>
                        <i class="am-icon-angle-right tpl-left-nav-more-ico am-fr am-margin-right tpl-left-nav-more-ico-rotate"></i>
                        </a>
                        <ul class="tpl-left-nav-sub-menu" style="display: block;">
                            <li>
                                <a href="xiugaimm.jsp">
                                    <span>修改密码</span>
                                </a>
                                <a href="gerengzx.jsp">
                                    <span>个人信息</span>
                                 </a>
                    
                            </li>
                        </ul>
                    </li>
					<li class="tpl-left-nav-item">
					    <a href="login.jsp" class="nav-link tpl-left-nav-link-list" style="background-color: gainsboro;">
					        <i class="am-icon-key"></i>
					        <span>退出登陆</span>
					
					    </a>
					</li>
                </ul>
            </div>
        </div>
		
		
		
		考试
		<div class="tpl-portlet-components" style="overflow:auto; border-radius:8px;height: 505px;">
		<br><font size="4">今日待考</font><br>
			<ul>
			
			<%
			
			if(!request.getSession().getAttribute("list").toString().equals("[]")){
				ArrayList<PublishExamPlan> list=(ArrayList<PublishExamPlan>)request.getSession().getAttribute("list");
			%>
			<%
				for(PublishExamPlan p:list){
			%>
			
				<li style="">
					<div style="border-radius:15px;padding: 10px;border: 1px solid lightskyblue;width: 100%;height: 100px;float: left;">
						<div style="float: left;">
							<img src="img/sj.png" style="border-radius:10px;border: 0px solid salmon;width: 80px;height: 80px;"/>
					
						</div>
						<div style="margin-left: 5px; float: left; border: 0px solid red;width: 830px;height: 80px;">
							<table>
								<tr>
									<td colspan="3">&nbsp;&nbsp;<b><%=p.getPaperName() %></b></td>
									<td rowspan="3" align="left" style="width: 400px;"><br />
										<div style="width: 400px;margin-top:-20px; border:0px solid red;">
										<font size="2" style="margin-left:20%;">
											开始时间：
											<%=p.getStartTime() %>
											
										</font>
											<img style="width: 30px;position: absolute;">
											<a style="float:right;left:20%;top:20px;position: relative;" onclick="ksks(this)" my="<%=p.getPaperName() %>" start="<%=p.getStartTime() %>" end="<%=p.getEndTime() %>" href="#">开始考试</a><br>
											<div style="height:10px;"></div>
											<font size="2" style="margin-left:20%;">
											结束时间：
											<%=p.getEndTime() %>
											
										</font>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr style="font-size: 14px">
									<td><img style="width:20px;height:20px;" src="img/shijian.png">考试时长：<%=p.getSj() %>分钟</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;<img style="width:20px;height:20px;" src="img/ts.png">共计：<%=p.getTscount() %>道</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;<img style="width:20px;height:20px;" src="img/fs.png">总分：<%=p.getTscount()*p.getFz() %>分</td>
								</tr>
							</table>
						</div>
					</div>
				</li>
				<li>
					<div style="width:10px;">&nbsp;</div>
				</li>
				<%} %>
			<%}else{%>
				<ul>
					<li>
						<div style="border-radius:15px;padding: 10px;border: 1px solid #999;width: 100%;height: 100px;float: left;">
							<div align="center" style="margin-top: 25px;color:#999" >暂无考试</div>
						</div>
					</li>
					<li>
						<div style="width:10px;">&nbsp;</div>
					</li>
				</ul>
			<%} %>
			</ul>
			<font size="4">其他考试</font><br>
			
				<ul>
					<li>
						<div style="border-radius:15px;padding: 10px;border: 1px solid #999;width: 100%;height: 100px;float: left;">
							<div align="center" style="margin-top: 25px;color:#999" >暂无考试</div>
						</div>
					</li>
					<li>
						<div style="width:10px;">&nbsp;</div>
					</li>
				</ul>
		</div>
		
    </div>
    <script src="http://www.jq22.com/jquery/jquery-2.1.1.js"></script>
    <script src="assets/js/amazeui.min.js"></script>
    <script src="assets/js/iscroll.js"></script>
    <script src="assets/js/app.js"></script>
</body>

</html>