<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>在线考试后台</title>
	 <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <link rel="stylesheet" href="css/style.css">
 <link rel="stylesheet" href="css/hsycmsAlert.min.css">
    <link rel="icon" type="image/png" href="assets/i/favicon.png">
 <script src="js/hsycmsAlert.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
	<script>
	<% request.getSession().setAttribute("ts", "0"); %>
		//添加面板
		function addTabs(title,url)
		{
			//得到面板是否存在，fag 是 boolean
			var fag=$('#tt').tabs('exists',title);
			if(fag)
			{
				//存在，则选中面板
				$('#tt').tabs('select',title);
			}else
			{
				//不存在，添加面板
				$('#tt').tabs('add',{
				    title:title,
				    href:url,
				    closable:true
				});	
			}
			
		}
	
	</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false"  style="height:60px;background-image:url(img/WechatIMG36.png);background-size:100%;padding: 12px;">
		<font size="5" style="color:white;margin-left: 1%">在线考试后台管理</font>
		<a style="float: right;position: relative;color:white;top:10px;text-decoration: none;font-size: 18px" href="login.jsp">退出</a>
	</div>
	<div data-options="region:'west',split:true,title:'菜单栏'" style="width:129px;">
		<div id="aa" class="easyui-accordion" data-options="fit:true">
		    
		    <div title="班级管理" data-options="iconCls:'icon-banji'" style="">
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('添加班级','addclass.jsp')">添加班级</a><br>
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('班级信息','class.jsp')">班级信息</a>
		    </div>
		    <div title="学生管理" data-options="iconCls:'icon-xues'" style="">
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('添加学生','addstudent.jsp')">添加学生</a><br>
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('学生信息','student.jsp')">学生信息</a>
		    </div>
		    <div title="科目管理" data-options="iconCls:'icon-kemu'" style="">
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('添加科目','addcurriInfo.jsp')">添加科目</a><br>
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('科目信息','curriInfo.jsp')">科目信息</a>
		    </div>
		    <div title="题库管理" data-options="iconCls:'icon-timu'" style="">
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('添加题目','addquestion.jsp')">添加题目</a><br>
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('题库信息','question.jsp')">题库信息</a>
		    </div>
		     <div title="试卷管理" data-options="iconCls:'icon-sj'" style="">
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('组卷','Composingatestpaper.jsp')">组卷</a><br>
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('添加试卷','addpaperInfo.jsp')">添加试卷</a>
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('试卷信息','paperInfo.jsp')">试卷信息</a>
		    </div>
		     <div title="考试管理" data-options="iconCls:'icon-gr'" style="">
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('考试计划','publishExamplan.jsp')">考试计划</a><br>
				<a class="easyui-linkbutton" data-options="plain:true" style="width:120px;" href="javaScript:addTabs('成绩查询','chengjicx.jsp')">成绩查询</a>
		    </div>
		</div>
	</div>
	<div data-options="region:'center'">
		<div id="tt" class="easyui-tabs" data-options="fit:true">
		    <div title="欢迎" style="padding:20px;display:none;">
		   		<div aling="center" style="margin-left:20%; margin-top:15%;">
					<font size="6" color="red">欢迎使用学生考试后台管理系统！</font>
				</div>
		    </div>
		</div>
	</div>
</body>
</body>
</html>
