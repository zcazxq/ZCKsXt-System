
function test(id) {
    if(id!=""){
        $.ajax({
            type : 'POST',
            url : 'QuestionInfoServlet?op=chakan',
            dataType : "Json",
            success : function(data){
            var titleB = data.title;
            var exam = data.exam;
            var test_box = '';

            $.each(exam, function(h, exam) {
                var title = exam.title;
                var info = exam.infos!=null?'<h4 class="jxz-title">'+exam.infos+'</h4>':'';
                var test = exam.values;
                var topic_box = '';

                $.each(test, function(i, topic) {//1单选
                    var type = topic.type;
                    var options = topic.options;
                    var answer = topic.answer;
                    var analysis = topic.analysis==null||topic.analysis==""?"无":topic.analysis;
                    var option_box = '';//题目
                    var paperid=topic.paperId;//试卷Id
                    var timuid=topic.timuId;//试题Id;
					
					
                    if(type==1){
                        $.each(options, function(j, option) {
                            option_box += `
                                    <div class="jxz-option radio">
                                        <label>
                                            <input name="test`+h+''+i+`" type="radio" value="`+j+`"> `+j+`：`+option+`
                                        </label>
                                    </div>
                                    `;
                        });
                        option_box+=`
                        	<a style="text-decoration:none;cursor:pointer" onclick="myYichu(`+paperid+`,`+timuid+`)">移除试题</a>
                        `;
                    }

                    var answer_op = '';//答案
                    if(type==3){
                        $.each(answer, function(i, aw) {
                            answer_op += aw==1 ? "正确" : "错误";
                        });
                    }else{
                        $.each(answer, function(i, aw) {
                            answer_op += answer.length==(i+1) ? aw : aw+" ";
                        });
                    }
                    topic_box += `
                            <div class="testCon" data-type="`+type+`" data-answer="`+answer_op+`">
                                <h4 class="jxz-title">`+topic.questionStem+`</h4>
                                `+option_box+`
                                <div class="topic-answer">
                                   <p>您的答案：<span class="userAnswer"></span></p>
                                   <p>正确答案：`+answer_op+`</p>
                                   <p>解析：`+analysis+`</p>
                                </div>
                            </div>
                            `;

                });
                test_box += `
                            <div class="jxz-box col-md-12">
                            <h4 class="tesTitle">`+title+`</h4>
                            `+info+`
                            `+topic_box+`
                        </div>
                        `;
            });

            var test_html = `
                    <div class="page-header">
                        <h3 class="text-center">`+titleB+`</h3>
                    </div>
                    <form class="" id="testForm">
                        <div class="test-form-box">
                            `+test_box+`
                        </div>
                    </form>`;
            $('#testArea').html(test_html);
   		 }
    	});
   	 }
}