<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.model.*,com.dao.*,java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	 <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<script>
$(function(){
	 $('#pepname').combobox({
			editable:false,
			onChange:function(){
				$('#pepdg').datagrid('load',{
		  			name:$("#pepname").val()
		  		});
			 }
		});
	$('#pepdg').datagrid({
		rownumbers:true,
		singleSelect:true,
		pagination:true,
		toolbar:'#pepbb',
		pageSize:'10',
		fitColumns:true,
		fit:true,
		pageList:[5,10,15,20,35,40],
	    url:'PublishExamPlanServlet?op=select',
	    columns:[[
			{field:'paperName',title:'试卷名称',width:100,editor:"validatebox"},
			{field:'className',title:'班级名称',width:100,editor:"validatebox"},
			{field:'startTime',title:'开始时间',width:100,editor:"validatebox"},
			{field:'endTime',title:'结束时间',width:100,editor:"validatebox"},
			{field:'creatTime',title:'创建时间',width:100},
			{field:'action',title:'操作',width:70,align:'center',
				formatter:function(value,row,index){
					if (row.editing){
						
					} else {
						var e = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onClick="chakansss('+row.paperId+')">预览</a></button>';
						var c = '<span style="width:40px;"> </span>';
						var a = "<button style='border:0px;'><a style='text-decoration:none;' href='#' onClick=\"puuudeleterows(this,"+row.publishExamPlanId+",'"+row.startTime+"','"+row.endTime+"')\">删除</a></button>";
						return e+c+a;
					}
				}
			}
	    ]],
	 onEndEdit:function(index,row){
			$.ajax
		    ({
		    	url: "PublishExamPlanServlet",
				dataType: "text",
				type: "post",
				data:"op=update&id="+row.classId+"&name="+row.className,
			    success:function(data){
				    if(data=="修改成功"){
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
			    	$('#pepdg').datagrid('load');
				}
		    });
		},
		onBeforeEdit:function(index,row){
			row.editing = true;
			$(this).datagrid('refreshRow', index);
		},
		onAfterEdit:function(index,rowData){
			rowData.editing = false;
			$(this).datagrid('refreshRow', index);
			id=rowData.userId;
		},
		onCancelEdit:function(index,row){
			row.editing = false;
			$(this).datagrid('refreshRow', index);
		}
	});
});
function getpepRowIndexs(target){
	var tr = $(target).closest('tr.datagrid-row');
	return parseInt(tr.attr('datagrid-row-index'));
}
function pepeditrows(target){
	$('#pepdg').datagrid('beginEdit', getpepRowIndexs(target));
}
function puuudeleterows(target,id,start,end){
	var sttime=new Date(start).getTime();
	var endtime=new Date(end).getTime();
	var date=new Date().getTime();
	if(sttime<date&&endtime>date){
		$.messager.alert('温馨提示',"该试卷正在考试,不能进行删除");	
	}else{
		 $.messager.confirm('删除','确定删除吗?',function(r){
			if (r){
				$('#pepdg').datagrid('deleteRow', getpepRowIndexs(target));
				$.ajax({
			    	url: "PublishExamPlanServlet",
					dataType: "text",
					type: "post",
					data:"op=delete&id="+id,
				    success:function(data){
					    if(data=="删除成功"){
					    	$('#pepdg').datagrid('load');
					    	$.messager.alert('温馨提示',data);	
					    }else{
					    	$.messager.alert('温馨提示',data+",请检查后重试！");	
					    }
					}
			    });
			}
		}); 
	}
}
function pepsaverows(target){
	$('#dg').datagrid('endEdit', getpepRowIndexs(target));
}
function pepcancelrows(target){
	$('#dg').datagrid('cancelEdit', getpepRowIndexs(target));
}



function chakansss(sjid){
		$.ajax({
	    	url: "QuestionInfoServlet",
			dataType: "text",
			type: "post",
			data:"op=hqid&sjid="+sjid,
		    success:function(data){
		    	window.open("chakan.jsp");
			}
	    });
}
</script>
<%
	PublishExamPlanDao pe=new PublishExamPlanDao();
	ArrayList<String> list=pe.selectAllName();
%>
		<div id="pepbb">
		试卷名称：
			<select id="pepname" class="easyui-combobox" name="dept" style="width:200px;height:34px;">
			   	<option value="0">全部试卷</option>
			    <% for(String l:list){ %>
			    <option value="<%=l %>"><%=l %></option>
				<%}%>
			</select>
		</div>
	<table id="pepdg"></table>
</body>
</html>