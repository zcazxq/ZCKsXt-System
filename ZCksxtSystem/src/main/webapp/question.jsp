<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.dao.*,com.model.*,java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	 <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<script>
<% 
	CurriInfoDao cid=new CurriInfoDao();
	ArrayList<CurriInfo> lists=cid.selectAll();
%>
var curriInfo=[
	<% for(CurriInfo l:lists){ %>
	{userA:'<%=l.getCurName() %>',userB:'<%=l.getCurName() %>'},
	<%}%>
];
var daan=[
	{name:'A',value:'A'},
	{name:'B',value:'B'},
	{name:'C',value:'C'},
	{name:'D',value:'D'}
];
$(function(){
	 $("#qubtn").click(function(){
			$('#qudgs').datagrid('load',{
				km:$("#qucc").val(),
	  			name:$("#qunames").val()
	  		});
	 });
	 $('#qucc').combobox({
			editable:false,
			onChange:function(){
				$('#qudgs').datagrid('load',{
					km:$("#qucc").val(),
		  			name:$("#qunames").val()
		  		});
			 }
		});
	 $('#qufile').filebox({
         buttonText: '选择文件',
         buttonAlign: 'right'
     })
	 $('#quffs').form({
		 url:"QuestionInfoServlet?op=adds",
		 dataType:'text',
			success:function(data){
				$('#qudgs').datagrid('reload');
				$('#qudgs').datagrid('load',{});
				$.messager.alert('温馨提示', data);
				
			}
		});
	$('#qudgs').datagrid({
		rownumbers:true,
		singleSelect:true,
		pagination:true,
		toolbar:'#qubbs',
		pageSize:'10',
		fitColumns:true,
		fit:true,
		pageList:[5,10,15,20,35,40],
	    url:'QuestionInfoServlet?op=select',
	    columns:[[
			{field:'curName',title:'科目名称',
					formatter:function(value,row){
						return row.productname || value;
					},
					editor:{
						type:'combobox',
						options:{
							valueField:'userB',
							textField:'userA',
							data:curriInfo,
							required:true
							}
						}
					},
			{field:'timuName',title:'题目',width:150,editor:"validatebox"},
			{field:'a',title:'A',width:90,editor:"validatebox"},
			{field:'b',title:'B',width:90,editor:"validatebox"},
			{field:'c',title:'C',width:90,editor:"validatebox"},
			{field:'d',title:'D',width:90,editor:"validatebox"},
			{field:'yes',title:'参考答案',width:60,
				formatter:function(value,row){
					return row.productname || value;
				},
				editor:{
					type:'combobox',
					options:{
						valueField:'name',
						textField:'value',
						data:daan,
						required:true
						}
					}
				},
			{field:'jieXi',title:'解析',width:100,editor:"validatebox"},
			{field:'fenZhi',title:'分值',width:50,editor:"numberspinner"},
			{field:'action',title:'操作',width:100,align:'center',
				formatter:function(value,row,index){
					if (row.editing){
						var s = '<button style="border:0px;"><a style="text-decoration:none;" href="#" class="easyui-linkbutton" data-options="plain:true" style="width:120px;" onclick="qusaverow(this)">保存</a></button>';
						var a = '<span style="width:20px;"> </span>';
						var c = '<button style="border:0px;"><a style="text-decoration:none;" href="#" class="easyui-linkbutton" data-options="plain:true" style="width:120px;" onclick="qucancelrow(this)">取消</a></button>';
						return s+a+c;
					} else {
						var e = '<button style="border:0px;"><a style="text-decoration:none;" href="#" class="easyui-linkbutton" data-options="plain:true" style="width:120px;" onclick="queditrow(this)">修改</a></button>';
						var a = '<span style="width:20px;"> </span>';
						var d = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="qudeleterow(this,'+row.timuId+')">删除</a></button>';
						return e+a+d;
					}
				}
			}
	    ]],
	 onEndEdit:function(index,row){
			$.ajax({
		    	url: "QuestionInfoServlet",
				dataType: "text",
				type: "post",
				data:"op=update&curName="+row.curName+"&A="+row.a+"&B="+row.b+"&C="+row.c+"&D="+row.d+"&yes="+row.yes+"&jieXi="+row.jieXi+"&fenZhi="+row.fenZhi+"&timuId="+row.timuId+"&timuName="+row.timuName,
			    success:function(data){
				    if(data=="修改成功"){
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
			    	$('#qudgs').datagrid('load');
				}
		    });
		},
		onBeforeEdit:function(index,row){
			row.editing = true;
			$(this).datagrid('refreshRow', index);
		},
		onAfterEdit:function(index,rowData){
			rowData.editing = false;
			$(this).datagrid('refreshRow', index);
			id=rowData.userId;
		},
		onCancelEdit:function(index,row){
			row.editing = false;
			$(this).datagrid('refreshRow', index);
		}
	});
});
function getquRowIndex(target){
	var tr = $(target).closest('tr.datagrid-row');
	return parseInt(tr.attr('datagrid-row-index'));
}
function queditrow(target){
	$('#qudgs').datagrid('beginEdit', getquRowIndex(target));
}
function qudeleterow(target,id){
	$.messager.confirm('删除','确定删除吗?',function(r){
		if (r){
			$('#qudgs').datagrid('deleteRow', getquRowIndex(target));
			$.ajax({
		    	url: "QuestionInfoServlet",
				dataType: "text",
				type: "post",
				data:"op=delete&id="+id,
			    success:function(data){
				    if(data=="删除成功"){
				    	$('#dgs').datagrid('load');
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
				}
		    });
		}
	});
}
function qusaverow(target){
	$('#qudgs').datagrid('endEdit', getquRowIndex(target));
}
function qucancelrow(target){
	$('#qudgs').datagrid('cancelEdit', getquRowIndex(target));
}
function myquDr(){
	$('#quwins').window('open'); 
}
function myquDc(){
	$('#quwin').window('open'); 
}
</script>
<div id="quwin" class="easyui-window" title="导出题库" style="width:500px;height:180px"
    data-options="iconCls:'icon-redo',modal:true,closed:true,draggable:false,resizable:false,minimizable:false,maximizable:false,collapsible:false,">
    <div align="center">
		<form id="quffss" action="QuestionInfoServlet?op=导出" method="post">
	    	<div style="margin-top: 25px">
		   		<select id="quccs" class="easyui-combobox" data-options="prompt:'请选择科目',editable:false," name="classnames" style="width:200px;height:34px;">
				   	<option value="全部科目">全部科目</option>
					   	<% for(CurriInfo l:lists){ %>
					   		<option><%=l.getCurName() %></option>
						<%}%>
				</select><br>
				<font style="color:red;">全部导出将按科目排序导出！</font><br>
			</div>
			<a class="easyui-linkbutton">
					<input id="qudcs" type="submit" value="导出" style="background-color:transparent;border:0px;width:100px;height:30px;">
				</a>
		</form>
	</div>
</div>
<div id="quwins" class="easyui-window" title="导入试题" style="width:500px;height:180px"
    data-options="iconCls:'icon-undo',modal:true,closed:true,draggable:false,resizable:false,minimizable:false,maximizable:false,collapsible:false,">
    <div align="center">
   		<div style="position:relative;" align="center">
   		<div style="position:absolute;float:left;right:2%;">
			<form id="quffss" action="QuestionInfoServlet?op=导入模版" method="post">
					<a onClick="myDrMb()" class="easyui-linkbutton" data-options="iconCls:'icon-help',plain:true">
						<input type="submit" value="下载模版" style="border: 0px;background-color:transparent;height:34px;">
					</a>
			</form>
		</div>
			<form id="quffs" method="post" enctype="multipart/form-data">
				<div style="margin-top: 25px">
					<input id="qufile" name="qufile" style="width:200px;height:34px;"><br>
					<font style="color:red;">请严格按照导入模版进行导入,谢谢您的配合！</font><br>
				</div>
				<a class="easyui-linkbutton">
					<input type="submit" value="导入" style="background-color:transparent;border:0px;width:100px;height:30px;">
				</a>
			</form>
		</div>
		</div>
	</div>
</div>
		<div id="qubbs">
			科目名称：
			<select id="qucc" class="easyui-combobox" data-options="prompt:'请选择科目',editable:false," name="qudept" style="width:200px;height:34px;">
				<option value="0">全部科目</option>
			   	<% for(CurriInfo l:lists){ %>
			   		<option value="<%=l.getCurId() %>"><%=l.getCurName() %></option>
				<%}%>
			</select>
			题目：
			<input name="qunames" id="qunames" class="easyui-textbox" prompt="请输入题目（选填）"  style="width:200px;height:34px;padding:10px">
				<a href="#" id="qubtn" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
				<a href="javaScript:addTabs('添加题目','addquestion.jsp')" id="quadds" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">添加</a>
					
				<a onClick="myquDr()" class="easyui-linkbutton" data-options="iconCls:'icon-undo',plain:true">导入</a>
				
				<a onClick="myquDc()" class="easyui-linkbutton" data-options="iconCls:'icon-redo',plain:true">导出</a>
		</div>
	<table id="qudgs"></table>
</body>
</html>