<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.dao.*,java.util.*,com.model.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>

</head>
<body>
	<div alegin="center" style="margin-top:10%">
				<form id="paff" action="PaperInfoServlet?op=add" method="post">
					<table align="center" style="padding:10px;">
						<tr>
							<% 
								CurriInfoDao cd=new CurriInfoDao();
								ArrayList<CurriInfo> list=cd.selectAll();
							%>
							<td>科目名称：</td>
							<td>
								<select name="pacuname" id="pacc" data-options="editable:false,prompt:'请选择科目名称'" class="easyui-combobox" required="required" style="width:300px;height:34px;">
							  		<option value="0"></option>
							  		<%for(CurriInfo l:list){ %>
							  		<option value="<%=l.getCurId() %>"><%=l.getCurName() %></option>
							  		<%} %>
								</select><br><br>	
							</td>
						</tr>
						<tr>
							<td>试卷名称：</td>
							<td>
								<input name="paname" data-options="required:true,multiline:true," class="easyui-textbox" prompt="请输入试卷名称" data-options="iconCls:'icon-man'" style="width:300px;height:60px;padding:10px"><br><br>
							</td>
						</tr>
						<tr align="center">
							<td colspan="2">
								<a class="easyui-linkbutton">
									<input type="submit" value="录入" style="background-color:transparent;border:0px;width:100px;height:30px;">
								</a>
							</td>
						</tr>
					</table>
				</form>		
			</div>
	<script>
	  	$(function(){
	  		$('#paff').form({
	     		success:function(data){
	     			if(data=="添加成功"){
		     			$.messager.alert('温馨提示',data);
	     			}else{
	     				$.messager.alert('温馨提示',data+"请检查后重试！");
	     			}
			    	$('#padg').datagrid('load');
	     		}
	     	});
	  		
	  	});
  </script>
</body>
</html>