<%@page import="com.alibaba.druid.pool.vendor.SybaseExceptionSorter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.dao.*,com.model.*,java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	 <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<script>

<% 
	CurriInfoDao cid=new CurriInfoDao();
	ArrayList<CurriInfo> lists=cid.selectAll();
%>
var curriInfo=[
	<% for(CurriInfo l:lists){ %>
	{userA:'<%=l.getCurName() %>',userB:'<%=l.getCurName() %>'},
	<%}%>
];
var daan=[
	{name:'A',value:'A'},
	{name:'B',value:'B'},
	{name:'C',value:'C'},
	{name:'D',value:'D'}
];
var ts;
$(function(){
	$('#ccc').combobox({
		editable:false,
		prompt:"请先选择科目",
		onChange:function(){
			$("#cqunames").val();
			 $('#cqudgs').datagrid('load',{
				 sjid:$("#ccc").val(),
		    	 km:$("#cqucc").val()
		  	 });
			 $.ajax({
			    	url: "QuestionInfoServlet",
					dataType: "text",
					type: "post",
					data:"op=cxts&sjid="+$("#ccc").val(),
				    success:function(data){
				    	 ts=data;
						 $("#ts").text(data+"/50");
					}
			    });
			
		 }
	});
	$('#cqucc').combobox({
		editable:false,
		onChange:function(){
			 $('#cqudgs').datagrid('load',{
				 km:$("#cqucc").val(),
		  		 name:$("#cqunames").val()
		  	 });
			 $('#ccc').combobox({
				 	prompt:"请选择试卷",
				    url:'QuestionInfoServlet?op=sjbyid&km='+$(this).val(),
				    valueField:'paperId',
				    textField:'paperName'
			});
		 }
	});
	 $("#cqubtn").click(function(){
			$('#cqudgs').datagrid('load',{
				sjid:$("#ccc").val(),
				km:$("#cqucc").val(),
	  			name:$("#cqunames").val()
	  		});
	 });
	 $('#cqufile').filebox({
         buttonText: '选择文件',
         buttonAlign: 'right'
     })
	 $('#cquffs').form({
		 url:"QuestionInfoServlet?op=adds",
		 dataType:'text',
			success:function(data){
				$('#cqudgs').datagrid('reload');
				$('#cqudgs').datagrid('load',{});
				$.messager.alert('温馨提示', data);
				
			}
		});
	$('#cqudgs').datagrid({
		rownumbers:true,
		singleSelect:true,
		pagination:true,
		toolbar:'#cqubbs',
		pageSize:'10',
		fitColumns:true,
		fit:true,
		pageList:[5,10,15,20,35,40],
	    url:'QuestionInfoServlet?op=selectBystate',
	    columns:[[
			{field:'curName',title:'科目名称',
					formatter:function(value,row){
						return row.productname || value;
					},
					editor:{
						type:'combobox',
						options:{
							valueField:'userB',
							textField:'userA',
							data:curriInfo,
							required:true
							}
						}
					},
			{field:'timuName',title:'题目',width:150,editor:"validatebox"},
			{field:'a',title:'A',width:90,editor:"validatebox"},
			{field:'b',title:'B',width:90,editor:"validatebox"},
			{field:'c',title:'C',width:90,editor:"validatebox"},
			{field:'d',title:'D',width:90,editor:"validatebox"},
			{field:'yes',title:'参考答案',width:60,
				formatter:function(value,row){
					return row.productname || value;
				},
				editor:{
					type:'combobox',
					options:{
						valueField:'name',
						textField:'value',
						data:daan,
						required:true
						}
					}
				},
			{field:'jieXi',title:'解析',width:100,editor:"validatebox"},
			{field:'fenZhi',title:'分值',width:50,editor:"numberspinner"},
			{field:'action',title:'操作',width:100,align:'center',
				formatter:function(value,row,index){
					var e = '<button style="border:0px;"><a style="text-decoration:none;" href="#" class="easyui-linkbutton" data-options="plain:true" style="width:120px;" onclick="cc(this,'+row.timuId+')">添加</a></button>';
					return e;
				}
			}
	    ]]
	});
});
function getquRowIndexc(target){
	var tr = $(target).closest('tr.datagrid-row');
	return parseInt(tr.attr('datagrid-row-index'));
}
function cc(target,id){
	if($("#cqucc").val()==0||$("#ccc").val()==""){
		$.messager.alert('温馨提示',"请先选择科目及试卷！");	
	}else{
		$.ajax({
	    	url: "QuestionInfoServlet",
			dataType: "text",
			type: "post",
			data:"op=updateBystate&id="+id+"&sjid="+$("#ccc").val(),
		    success:function(data){
			    if(data=="添加试题失败"){
			    	$.messager.alert('温馨提示',data+",请检查后重试！");	
			    }else{
			    	var ss=parseInt(ts)+1
			    	ts=ss;
			    	$("#ts").text(ss+"/50");
			    }
		    	$('#cqudgs').datagrid('load',{
		    		pd:"添加",
		    		sjid:$("#ccc").val(),
			    	km:$("#cqucc").val()
		    	});
			}
	    });
	}
	$("#ts").text(${ts}+"/50");
}
function myquDrc(){
	$('#cquwins').window('open'); 
}
function myquDcc(){
	$('#cquwin').window('open'); 
}
//预览试卷
function chakan(){
	if($("#cqucc").val()==0||$("#ccc").val()==""){
		$.messager.alert('温馨提示',"请先选择科目及试卷！");	
	}else{
		$.ajax({
	    	url: "QuestionInfoServlet",
			dataType: "text",
			type: "post",
			data:"op=hqid&sjid="+$("#ccc").val(),
		    success:function(data){
		    	window.open("chakan.jsp"); 
			}
	    });
	}
}

</script>
<div id="cquwin" class="easyui-window" title="导出试卷" style="width:500px;height:180px"
    data-options="iconCls:'icon-redo',modal:true,closed:true,draggable:false,resizable:false,minimizable:false,maximizable:false,collapsible:false,">
    <div align="center">
		<form id="cquffss" action="QuestionInfoServlet?op=导出" method="post">
	    	<div style="margin-top: 25px">
		   		<select id="cquccs" class="easyui-combobox" name="classnames" style="width:200px;height:34px;">
				   	<option value="全部科目">全部科目</option>
					   	<% for(CurriInfo l:lists){ %>
					   		<option><%=l.getCurName() %></option>
						<%}%>
				</select><br>
				<font style="color:red;">全部导出将按科目排序导出！</font><br>
			</div>
			<a class="easyui-linkbutton">
					<input id="cqudcs" type="submit" value="导出" style="background-color:transparent;border:0px;width:100px;height:30px;">
				</a>
		</form>
	</div>
</div>
<div id="cquwins" class="easyui-window" title="导入试卷" style="width:500px;height:180px"
    data-options="iconCls:'icon-undo',modal:true,closed:true,draggable:false,resizable:false,minimizable:false,maximizable:false,collapsible:false,">
    <div align="center">
   		<div style="position:relative;" align="center">
   		<div style="position:absolute;float:left;right:2%;">
			<form id="cquffss" action="QuestionInfoServlet?op=导入模版" method="post">
					<a onClick="myDrMbc()" class="easyui-linkbutton" data-options="iconCls:'icon-help',plain:true">
						<input type="submit" value="导入模版" style="border: 0px;background-color:transparent;height:34px;">
					</a>
			</form>
		</div>
			<form id="cquffs" method="post" enctype="multipart/form-data">
				<div style="margin-top: 25px">
					<input id="cqufile" name="cqufile" style="width:200px;height:34px;"><br>
					<font style="color:red;">请严格按照导入模版进行导入,谢谢您的配合！</font><br>
				</div>
				<a class="easyui-linkbutton">
					<input type="submit" value="导入" style="background-color:transparent;border:0px;width:100px;height:30px;">
				</a>
			</form>
		</div>
		</div>
	</div>
</div>
	
		<div id="cqubbs">
		科目名称：
			<select id="cqucc" name="cqudept" class="easyui-combobox" style="width:200px;height:34px;">
			   	<option name="ids" vlaue="0">全部科目</option>
			   	<% for(CurriInfo l:lists){ %>
			   		<option value="<%=l.getCurId() %>"><%=l.getCurName() %></option>
				<%}%>
			</select>
			试卷名称：
			<input id="ccc" name="ccc" style="width:200px;height:34px;">
			题目：
			<input name="cqunames" id="cqunames" class="easyui-textbox" prompt="请输入题目（选填）"  style="width:200px;height:34px;padding:10px">
				<a href="#" id="cqubtn" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
					
				<a onClick="chakan()" class="easyui-linkbutton" data-options="iconCls:'',plain:true">预览试卷</a>
				<font id="ts">${ts }/50</font>
		</div>
	<table id="cqudgs"></table>
</body>
</html>