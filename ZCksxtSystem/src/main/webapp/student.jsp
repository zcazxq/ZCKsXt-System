<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.dao.*,com.model.Class,java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	 <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<script>
<% 
	ClassDao cd=new ClassDao();
	ArrayList<Class> list=cd.selectAll();
%>
var products=[
	<% for(Class l:list){ %>
	{userName:'<%=l.getClassName() %>',userValue:'<%=l.getClassName() %>'},
	<%}%>
];
var se=[
	{name:'男',value:'男'},
	{name:'女',value:'女'}
];
$(function(){
	 $("#btn").click(function(){
			$('#dgs').datagrid('load',{
				bj:$("#cc").val(),
	  			name:$("#names").val()
	  		});
	 });
	 $('#cc').combobox({
			editable:false,
			onChange:function(){
				$('#dgs').datagrid('load',{
					bj:$("#cc").val(),
		  			name:$("#names").val()
		  		});
			 }
		});
	 $('#file').filebox({
         buttonText: '选择文件',
         buttonAlign: 'right'
     })
	 $('#ffs').form({
		 url:"StuServlet?op=adds",
		 dataType:'text',
			success:function(data){
				$('#dgs').datagrid('reload');
				$('#dgs').datagrid('load',{});
				$.messager.alert('温馨提示', data);
				
			}
		});
	$('#dgs').datagrid({
		rownumbers:true,
		singleSelect:true,
		pagination:true,
		toolbar:'#bbs',
		pageSize:'10',
		fitColumns:true,
		fit:true,
		pageList:[5,10,15,20,35,40],
	    url:'StuServlet?op=select',
	    columns:[[
			{field:'className',title:'班级名称',width:30,
				formatter:function(value,row){
					return row.productname || value;
				},
				editor:{
					type:'combobox',
					options:{
						valueField:'userValue',
						textField:'userName',
						data:products,
						required:true
						}
					}
				},
			{field:'stuName',title:'学生姓名',editor:"validatebox"},
			{field:'stuPwd',title:'学生密码',editor:"validatebox"},
			{field:'stuSex',title:'学生性别',
				formatter:function(value,row){
					return row.productname || value;
				},
				editor:{
					type:'combobox',
					options:{
						valueField:'name',
						textField:'value',
						data:se,
						required:true
						}
					}
				},
			{field:'stuPhone',title:'联系电话',width:100,editor:"validatebox"},
			{field:'stuStatus',title:'身份证号',width:100,editor:"validatebox"},
			{field:'action',title:'操作',width:70,align:'center',
				formatter:function(value,row,index){
					if (row.editing){
						var s = '<button style="border:0px;"><a style="text-decoration:none;" href="#" class="easyui-linkbutton" data-options="plain:true" style="width:120px;" onclick="saverow(this)">确定</a></button>';
						var a = '<span style="width:40px;"> </span>';
						var c = '<button style="border:0px;"><a style="text-decoration:none;" href="#" class="easyui-linkbutton" data-options="plain:true" style="width:120px;" onclick="cancelrow(this)">取消</a></button>';
						return s+a+c;
					} else {
						var e = '<button style="border:0px;"><a style="text-decoration:none;" href="#" class="easyui-linkbutton" data-options="plain:true" style="width:120px;" onclick="editrow(this)">修改</a></button>';
						var a = '<span style="width:40px;"> </span>';
						var d = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="deleterow(this,'+row.stuId+')">删除</a></button>';
						return e+a+d;
					}
				}
			}
	    ]],
	 onEndEdit:function(index,row){
			$.ajax({
		    	url: "StuServlet",
				dataType: "text",
				type: "post",
				data:"op=update&classname="+row.className+"&stuid="+row.stuId+"&stuname="+row.stuName+"&stupwd="+row.stuPwd+"&stusex="+row.stuSex+"&stuphone="+row.stuPhone+"&stustatus="+row.stuStatus,
			    success:function(data){
				    if(data=="修改成功"){
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
			    	$('#dgs').datagrid('load');
				}
		    });
		},
		onBeforeEdit:function(index,row){
			row.editing = true;
			$(this).datagrid('refreshRow', index);
		},
		onAfterEdit:function(index,rowData){
			rowData.editing = false;
			$(this).datagrid('refreshRow', index);
			id=rowData.userId;
		},
		onCancelEdit:function(index,row){
			row.editing = false;
			$(this).datagrid('refreshRow', index);
		}
	});
});
function getRowIndex(target){
	var tr = $(target).closest('tr.datagrid-row');
	return parseInt(tr.attr('datagrid-row-index'));
}
function editrow(target){
	$('#dgs').datagrid('beginEdit', getRowIndex(target));
}
function deleterow(target,id){
	$.messager.confirm('删除','确定删除吗?',function(r){
		if (r){
			$('#dgs').datagrid('deleteRow', getRowIndex(target));
			$.ajax({
		    	url: "StuServlet",
				dataType: "text",
				type: "post",
				data:"op=delete&id="+id,
			    success:function(data){
				    if(data=="删除成功"){
				    	$('#dgs').datagrid('load');
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
				}
		    });
		}
	});
}
function saverow(target){
	$('#dgs').datagrid('endEdit', getRowIndex(target));
}
function cancelrow(target){
	$('#dgs').datagrid('cancelEdit', getRowIndex(target));
}
function myDr(){
	$('#wins').window('open'); 
}
function myDc(){
	$('#win').window('open'); 
}
function dao(){
	/* $('#win').window('close');
	hsycms.loading('正在下载');
	
	setInterval(function() { 
			hsycms.closeAll();
	}, 1000);  */
}
</script>
<div id="win" class="easyui-window" title="导出学生信息" style="width:500px;height:180px"
    data-options="iconCls:'icon-redo',modal:true,closed:true,draggable:false,resizable:false,minimizable:false,maximizable:false,collapsible:false,">
    <div align="center">
		<form id="ffss" action="StuServlet?op=导出"  method="post">
	    	<div style="margin-top: 25px">
		   		<select id="ccs" class="easyui-combobox" name="classnames" style="width:200px;height:34px;">
				   	<option value="全部班级">全部班级</option>
				    <% for(Class l:list){ %>
				    <option><%=l.getClassName() %></option>
					<%}%>
				</select><br>
				<font style="color:red;">全部导出将按班级排序的方式导出全部学生！</font><br>
			</div>
				<a class="easyui-linkbutton" onclick="dao()">
					<input type="submit" value="导出" style="background-color:transparent;border:0px;width:100px;height:30px;">
				</a>
		</form>
	</div>
</div>
<div id="wins" class="easyui-window" title="导入学生信息" style="width:500px;height:180px"
    data-options="iconCls:'icon-undo',modal:true,closed:true,draggable:false,resizable:false,minimizable:false,maximizable:false,collapsible:false,">
    <div align="center">
   		<div style="position:relative;" align="center">
   		<div style="position:absolute;float:left;right:2%;">
			<form id="ffss" action="StuServlet?op=导入模版" method="post">
					<a class="easyui-linkbutton" data-options="iconCls:'icon-help',plain:true">
						<input type="submit" value="下载模版" style="cursor:pointer;border: 0px;background-color:transparent;height:34px;">
					</a>
			</form>
		</div>
			<form id="ffs" method="post" enctype="multipart/form-data">
				<div style="margin-top: 25px">
					<input id="file" name="file" style="width:200px;height:34px;"><br>
					<font style="color:red;">请严格按照导入模版进行导入,谢谢您的配合！</font><br>
				</div>
				<a class="easyui-linkbutton">
					<input type="submit" value="导入" style="background-color:transparent;border:0px;width:100px;height:30px;">
				</a>
			</form>
		</div>
		</div>
	</div>
</div>
		<div id="bbs">
			班级名称：
			<select id="cc" class="easyui-combobox" name="dept" style="width:200px;height:34px;">
			   	<option value="0">全部班级</option>
			    <% for(Class l:list){ %>
			    <option value="<%=l.getClassId() %>"><%=l.getClassName() %></option>
				<%}%>
			</select>
			学生姓名：
			<input name="names" id="names" class="easyui-textbox" prompt="请输入学生姓名（选填）"  style="width:200px;height:34px;padding:10px">
				<a href="#" id="btn" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
				<a href="javaScript:addTabs('添加学生','addstudent.jsp')" id="adds" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">添加</a>
					
				<a onClick="myDr()" class="easyui-linkbutton" data-options="iconCls:'icon-undo',plain:true">导入</a>
				
				<a onClick="myDc()" class="easyui-linkbutton" data-options="iconCls:'icon-redo',plain:true">导出</a>
		</div>
	<table id="dgs"></table>
</body>
</html>