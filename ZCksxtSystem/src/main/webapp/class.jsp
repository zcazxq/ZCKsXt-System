<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	 <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<script>
$(function(){
	 $("#btn").click(function(){
			$('#dg').datagrid('load',{
	  			name:$("#name").val()
	  		});
		});
	$('#dg').datagrid({
		rownumbers:true,
		singleSelect:true,
		pagination:true,
		toolbar:'#bb',
		pageSize:'10',
		fitColumns:true,
		fit:true,
		pageList:[5,10,15,20,35,40],
	    url:'ClassServlet?op=select',
	    columns:[[
			{field:'className',title:'班级名称',width:100,editor:"validatebox"},
			{field:'action',title:'操作',width:70,align:'center',
				formatter:function(value,row,index){
					if (row.editing){
						var s = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="saverows(this)">保存</a></button>';
						var a = '<span style="width:20px;"> </span>';
						var c = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="cancelrows(this)">取消</a></button>';
						return s+a+c;
					} else {
						var e = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="editrows(this)">修改</a></button>';
						var a = '<span style="width:20px;"> </span>';
						var d = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="deleterows(this,'+row.classId+')">删除</a></button>';
						return e+a+d;
					}
				}
			}
	    ]],
	 onEndEdit:function(index,row){
			$.ajax
		    ({
		    	url: "ClassServlet",
				dataType: "text",
				type: "post",
				data:"op=update&id="+row.classId+"&name="+row.className,
			    success:function(data){
				    if(data=="修改成功"){
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
			    	$('#dg').datagrid('load');
				}
		    });
		},
		onBeforeEdit:function(index,row){
			row.editing = true;
			$(this).datagrid('refreshRow', index);
		},
		onAfterEdit:function(index,rowData){
			rowData.editing = false;
			$(this).datagrid('refreshRow', index);
			id=rowData.userId;
		},
		onCancelEdit:function(index,row){
			row.editing = false;
			$(this).datagrid('refreshRow', index);
		}
	});
});
function getRowIndexs(target){
	var tr = $(target).closest('tr.datagrid-row');
	return parseInt(tr.attr('datagrid-row-index'));
}
function editrows(target){
	$('#dg').datagrid('beginEdit', getRowIndexs(target));
}
function deleterows(target,id){
	$.messager.confirm('删除','确定删除吗?',function(r){
		if (r){
			$('#dg').datagrid('deleteRow', getRowIndexs(target));
			$.ajax({
		    	url: "ClassServlet",
				dataType: "text",
				type: "post",
				data:"op=delete&id="+id,
			    success:function(data){
				    if(data=="删除成功"){
				    	$('#dg').datagrid('load');
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
				}
		    });
		}
	});
}
function saverows(target){
	$('#dg').datagrid('endEdit', getRowIndexs(target));
}
function cancelrows(target){
	$('#dg').datagrid('cancelEdit', getRowIndexs(target));
}
</script>
		<div id="bb">
		班级名称：
			<input name="name" id="name" class="easyui-textbox" prompt="请输入班级名称"  style="width:200px;height:34px;padding:10px">
			<a href="#" id="btn" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a href="javaScript:addTabs('添加班级','addclass.jsp')" id="add" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">添加</a>
		</div>
	<table id="dg"></table>
</body>
</html>