<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.dao.*,com.model.*"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<title>在线考试</title>
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link href="css/iconfont.css" rel="stylesheet" type="text/css" />
<link href="css/test.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/hsycmsAlert.min.css">
    <link rel="icon" type="image/png" href="assets/i/favicon.png">

<style>
.hasBeenAnswer {
	background: #5d9cec;
	color:#fff;
}

</style>
</head>

<body>
<%
if(request.getSession().getAttribute("qu")!=null){ 
	ArrayList<QuestionInfo> list=(ArrayList<QuestionInfo>)request.getSession().getAttribute("qu");

%>
	<div class="main">
		<!--nr start-->
		<div class="test_main">
			<div class="nr_left">
				<div class="test">
					<form action="" method="post">
						<div class="test_title" style="text-align: center">
							<h2 style="margin-top: 5px"><%=list.get(0).getPaperName() %></h2>
						</div>
						
							<div class="test_content">
								<div class="test_content_title">
									<h2>单选题</h2>
									<p>
										<span>共</span><i class="content_lit"><%=list.size() %></i><span>题，</span><span>合计</span><i class="content_fs"><%=list.size()*Integer.parseInt(list.get(0).getFenZhi())  %></i><span>分</span>
									</p>
								</div>
							</div>
							<div class="test_content_nr">
								<ul>
									<%
										int i=1;
										for(QuestionInfo q:list){
									%>
										<li id="qu_0_<%=i-1 %>">
											<div class="test_content_nr_tt">
												<i><%=i %></i><span>(2分)</span><font><%=q.getTimuName() %></font>
											</div>

											<div class="test_content_nr_main">
												<ul>
													
														<li class="option">
															
																<input type="radio" class="radioOrCheck" name="answer<%=i %>"
																	id="0_answer_<%=i %>_option_1" value="A" yes="<%=q.getYes() %>"
																/>
															
															
															<label for="0_answer_<%=i %>_option_1">
																A.
																<p class="ue" style="display: inline;"><%=q.getA() %></p>
															</label>
														</li>
													
														<li class="option">
															
																<input type="radio" class="radioOrCheck" name="answer<%=i %>"
																	id="0_answer_<%=i %>_option_2" value="B" yes="<%=q.getYes() %>"
																/>
															
															
															<label for="0_answer_<%=i %>_option_2">
																B.
																<p class="ue" style="display: inline;"><%=q.getB() %></p>
															</label>
														</li>
													
														<li class="option">
															
																<input type="radio" class="radioOrCheck" name="answer<%=i %>"
																	id="0_answer_<%=i %>_option_3" value="C" yes="<%=q.getYes() %>"
																/>
															
															
															<label for="0_answer_<%=i %>_option_3">
																C.
																<p class="ue" style="display: inline;"><%=q.getC() %></p>
															</label>
														</li>
													
														<li class="option">
															
																<input type="radio" class="radioOrCheck" name="answer<%=i %>"
																	id="0_answer_<%=i %>_option_4" value="D" yes="<%=q.getYes() %>"
																/>
															
															
															<label for="0_answer_<%=i++ %>_option_4">
																D.
																<p class="ue" style="display: inline;"><%=q.getD() %></p>
															</label>
														</li>
													
												</ul>
											</div>
										</li>
									<%} %>
								</ul>
							</div>
						
					</form>
				</div>

			</div>
			<div class="nr_right">
				<div class="nr_rt_main">
					<div class="rt_nr1">
						<div class="rt_nr1_title">
							<h1 style="width: 280px;">
								<i class="icon iconfont">&#xe692;</i>答题卡
							</h1>
						</div>
						
							<div class="rt_content">
								<div class="rt_content_tt">
									<h2>单选题</h2>
									<p>
										<span>共</span><i class="content_lit"><%=list.size() %></i><span>题</span>
									</p>
								</div>
									<div class="rt_content_nr answerSheet">
										<ul>
											<%
												i=1;
												for(QuestionInfo q:list){
											%>
												<li><a href="#qu_0_<%=i-1 %>"><%=i++ %></a></li>
											<%} %>
										</ul>
									</div>
								<br>
								<div align="center">
							
							
								<table align="center">
								<tr>
									<td colspan="2">
										<font size="3">
											剩余时间：
										</font><br><br><br>
									</td>
									<td colspan="2">
										<p class="test_time">
											<b id="tim" class="alt-1"><%=list.get(0).getJgsj() %></b>
										</p><br><br>
									</td>
								</tr>
									<tr align="center">
										<td align="center"><img style="width:30px;height:30px" src="img/yes.png"></td>
										<td align="center">已做</td>
										<td><img style="width:30px;height:30px" src="img/no.png"></td>
										<td>未做</td>
									</tr>
								</table>
								<br>
									<font>
										<input id="jiaojue" style="border-radius: 40px;background:#389fc3; border:none; display:block; width:133px; height:33px; cursor:pointer; color:#fff; font-size:16px;" type="button" name="test_jiaojuan" value="交卷">
									</font>
									
								</div>
								<br>
								<br>
							</div>
						
							
					</div>

				</div>
			</div>
		</div>
		<!--nr end-->
		<div class="foot"></div>
	</div>

	<script src="http://www.jq22.com/jquery/jquery-1.10.2.js"></script>
	<script src="js/jquery.easy-pie-chart.js"></script>
 	<script src="js/hsycmsAlert.min.js"></script>
	<!--时间js-->
	<script src="js/jquery.countdown.js"></script>
	<script>
		window.jQuery(function($) {
			"use strict";
			
			$('time').countDown({
				with_separators : false
			});
			$('.alt-1').countDown({
				css_class : 'countdown-alt-1'
			});
			
		});
		function jj(){
			var json="";
			var cj=0;
			for(var i=1;i<=<%=list.size() %>;i++){
				if($("input[name=answer"+i+"]:checked").val()==undefined){
					json="";
					hsycms.tips("您还有未作答的题！",()=>{
					       console.log("提示关闭后");
					     },1500)
					break;
				}
				var da=$("input[name=answer"+i+"]:checked").val();
				json+="&"+i+"="+da;
				if($("input[name=answer"+i+"]:checked").val()==$("input[name=answer"+i+"]:checked").attr("yes")){
					cj+=<%=list.get(0).getFenZhi() %>;
				}
			}
			
			
			if(json==""){
				
			}else{
				hsycms.confirm('确定要交卷吗？',
				         function(res){     
							//点击了确定
							$.ajax({
						    	url: "AnswerRecordServlet",
								dataType: "text",
								type: "post",
								data:"op=jiaojuan&sjid=<%=list.get(0).getPaperId() %>&cj="+cj+json,
							    success:function(data){
								    location.href="jiaojuanhou.jsp";
								}
						    });
				         },
				        function(res){
				        	 //点击了取消
				         },
				      )
			}
		}

		function zdjj(){
			var json="";
			var cj=0;
			for(var i=1;i<=<%=list.size() %>;i++){
				var da="";
				if($("input[name=answer"+i+"]:checked").val()==undefined){
					da="";
				}else{
					da=$("input[name=answer"+i+"]:checked").val();
				}
				
				json+="&"+i+"="+da;
				if($("input[name=answer"+i+"]:checked").val()==$("input[name=answer"+i+"]:checked").attr("yes")){
					cj+=<%=list.get(0).getFenZhi() %>;
				}
			}
			$.ajax({
		    	url: "AnswerRecordServlet",
				dataType: "text",
				type: "post",
				data:"op=jiaojuan&sjid=<%=list.get(0).getPaperId() %>&cj="+cj+json,
			    success:function(data){
				    location.href="jiaojuanhou.jsp";
				}
		    });
		}
		$(function() {
			$("#jiaojue").click(function(){
				jj();
			});
			
			var tim=setInterval(function() { 
				if($("#tim").text()=="00:15:00"){
					hsycms.tips("请注意考试结束还有15分钟！",()=>{
					       console.log("提示关闭后");
					     },1500)
				}
				if($("#tim").text()=="00:00:00"){
					hsycms.tips("考试时间到，已自动帮您交卷！",()=>{
						jj();
					     },1500)
				}
					
			}, 1000); 
			
			$('li.option label').click(function() {
			debugger;
				var examId = $(this).closest('.test_content_nr_main').closest('li').attr('id'); // 得到题目ID
				var cardLi = $('a[href=#' + examId + ']'); // 根据题目ID找到对应答题卡
				// 设置已答题
				if(!cardLi.hasClass('hasBeenAnswer')){
					cardLi.addClass('hasBeenAnswer');
				}
				
			});
		});
	</script>

<%} %>
</body>

</html>