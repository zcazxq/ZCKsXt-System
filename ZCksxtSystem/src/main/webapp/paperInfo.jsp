<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.dao.*,com.model.*,java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	 <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/default/easyui.css">
  <link rel="stylesheet" type="text/css" href="jquery-easyui-1.7.0/themes/icon.css">
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.min.js"></script>
  <script type="text/javascript" src="jquery-easyui-1.7.0/jquery.easyui.min.js"></script>
  <script src="jquery-easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<script>
<% 
	CurriInfoDao cid=new CurriInfoDao();
	ArrayList<CurriInfo> lists=cid.selectAll();
%>
var curriInfos=[
	<% for(CurriInfo l:lists){ %>
		{userA:'<%=l.getCurName() %>',userB:'<%=l.getCurName() %>'},
	<%}%>
];
$(function(){
	$('#paiffs').form({
		url:"QuestionInfoServlet?op=fbks",
		dataType:"text",
		success:function(data){
			if(data=="seccuss"){
				$.messager.alert('温馨提示', "发布成功");
			}else if(data=="erro"){
				$.messager.alert('温馨提示', "发布失败");
			}else if(data=="erros"){
				$.messager.alert('温馨提示', "部分已发布成功！");
			}
			$("#ii").attr("disabled",true).css("pointer-events","none");
			$('#paiffs').trigger("reset");
			$("#cquccss").combobox("select","0");
			$('#pepdg').datagrid('load');
		}
	});
	$('#cccs').combobox({
		editable:false,
		prompt:"请先选择科目",
		onChange:function(){
			$('#fbbj').combobox({
				prompt:"请选择班级",
			    url:'QuestionInfoServlet?op=cxfbbj&sjid='+$('#cccs').val(),
			    valueField:'classId',
			    textField:'className'
			});
		}
	});
	$('#paccs').combobox({
		onChange:function(){
			$('#padg').datagrid('load',{
	  			name:$("#paname").val(),
	  			km:$("#paccs").val()
	  		});
		 }
	});
	$('#cquccss').combobox({
		editable:false,
		onChange:function(){
			 $('#cccs').combobox({
				 	prompt:"请选择试卷",
				    url:'QuestionInfoServlet?op=sjbyid&km='+$(this).val(),
				    valueField:'paperId',
				    textField:'paperName'
			});
		 }
	});
	$('#fbbj').combobox({
		editable:false
	});
	 $("#pabtn").click(function(){
			$('#padg').datagrid('load',{
	  			name:$("#paname").val(),
	  			km:$("#paccs").val()
	  		});
		});
	 $("#pabtns").click(function(){
		 $('#pawin').window('open');
	});
	$('#padg').datagrid({
		rownumbers:true,
		singleSelect:true,
		pagination:true,
		toolbar:'#pabb',
		pageSize:'10',
		fitColumns:true,
		fit:true,
		pageList:[5,10,15,20,35,40],
	    url:'PaperInfoServlet?op=select',
	    columns:[[
			{field:'curName',title:'科目名称',width:100,
				formatter:function(value,row){
					return row.productname || value;
				},
				editor:{
					type:'combobox',
					options:{
						valueField:'userB',
						textField:'userA',
						data:curriInfos,
						required:true
						}
					}
				},
			{field:'paperName',title:'试卷名称',width:100,editor:"validatebox"},
			{field:'createTime',title:'创建时间',width:100},
			{field:'action',title:'操作',width:70,align:'center',
				formatter:function(value,row,index){
					if (row.editing){
						var s = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="pasaverows(this)">保存</a></button>';
						var a = '<span style="width:20px;"> </span>';
						var c = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onclick="pacancelrows(this)">取消</a></button>';
						return s+a+c;
					} else {
						var f = '<button style="border:0px;"><a style="text-decoration:none;" href="#" onClick="chakans('+row.paperId+')">预览</a></button>';
						var e = '<button style="border:0px;"><a id="'+row.paperId+'" style="text-decoration:none;" href="#" onclick="paeditrows(this)">修改</a></button>';
						var a = '<span style="width:20px;"> </span>';
						var d = '<button style="border:0px;"><a id="'+row.paperId+'" style="text-decoration:none;" href="#" onclick="padeleterows(this,'+row.paperId+')">删除</a></button>';
						return f+a+e+a+d+a;
					}
				}
			}
	    ]],
	 onEndEdit:function(index,row){
			$.ajax
		    ({
		    	url: "PaperInfoServlet",
				dataType: "text",
				type: "post",
				data:"op=update&curname="+row.curName+"&papername="+row.paperName+"&paperid="+row.paperId,
			    success:function(data){
				    if(data=="修改成功"){
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
			    	$('#padg').datagrid('load');
				}
		    });
		},
		onBeforeEdit:function(index,row){
			row.editing = true;
			$(this).datagrid('refreshRow', index);
		},
		onAfterEdit:function(index,rowData){
			rowData.editing = false;
			$(this).datagrid('refreshRow', index);
			id=rowData.userId;
		},
		onCancelEdit:function(index,row){
			row.editing = false;
			$(this).datagrid('refreshRow', index);
		}
	});
});
function getpaRowIndexs(target){
	var tr = $(target).closest('tr.datagrid-row');
	return parseInt(tr.attr('datagrid-row-index'));
}
function paeditrows(target){
	$('#padg').datagrid('beginEdit', getpaRowIndexs(target));
}
function padeleterows(target,id){
	$.messager.confirm('删除','确定删除吗?',function(r){
		if (r){
			$('#padg').datagrid('deleteRow', getpaRowIndexs(target));
			$.ajax({
		    	url: "PaperInfoServlet",
				dataType: "text",
				type: "post",
				data:"op=delete&id="+id,
			    success:function(data){
				    if(data=="删除成功"){
				    	$('#padg').datagrid('load');
				    	$.messager.alert('温馨提示',data);	
				    }else{
				    	$.messager.alert('温馨提示',data+",请检查后重试！");	
				    }
				}
		    });
		}
	});
}
function pasaverows(target){
	$('#padg').datagrid('endEdit', getpaRowIndexs(target));
}
function pacancelrows(target){
	$('#padg').datagrid('cancelEdit', getpaRowIndexs(target));
}
//预览试卷
function chakans(sjid){
		$.ajax({
	    	url: "QuestionInfoServlet",
			dataType: "text",
			type: "post",
			data:"op=hqid&sjid="+sjid,
		    success:function(data){
		    	window.open("chakan.jsp");
			}
	    });
}
</script>
		<div id="pawin" class="easyui-window" title="发布考试" style="width:500px;height:450px"
		    data-options="modal:true,closed:true,collapsible:false,minimizable:false,maximizable:false">
		   <form id="paiffs" method="post">
					<table align="center" style="padding:10px;">
						<tr>
							<td>科目名称：</td>
							<td>
								<select id="cquccss" name="fbkm" class="easyui-combobox" style="width:200px;height:34px;">
								   	<option name="ids" value="0">请选择科目</option>
								   	<% for(CurriInfo l:lists){ %>
								   		<option value="<%=l.getCurId() %>"><%=l.getCurName() %></option>
									<%}%>
								</select><br><br>
							</td>
						</tr>
						<tr>
						<tr>
							<td>试卷名称：</td>
							<td>
								<input id="cccs" name="fbsj" style="width:200px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
							<% 
								ClassDao cl=new ClassDao();
								ArrayList<com.model.Class> list=cl.selectAll();
							%>
							<td>发布班级：</td>
							<td>
							<input id="fbbj" class="easyui-combobox" 
								name="fbbj"
								value=""
								data-options="
										prompt:'请选择班级',
										url:'ClassServlet?op=selectAll',
										method:'post',
										valueField:'classId',
										textField:'className',
										multiple:true,
										editable:false,
										panelHeight:'auto'
								" style="width:200px;height:34px;padding:10px"><br><br>
							</td>
						</tr>
						<tr>
							<td>考试日期：</td>
							<td>
								<input name="riqi" id="dds" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" style="width:200px;height:34px;padding:10px"></input><br><br>
							</td>
						</tr>
						<script type="text/javascript">
						$(function(){
							$('#dds').datebox().datebox('calendar').calendar({
								validator: function(date){
									var now = new Date();
									var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
									var d2 = new Date(now.getFullYear(), now.getMonth(), now.getDate()+15);
									return d1<=date && date<=d2;
								}
							});
						});
							function myformatter(date){
								var y = date.getFullYear();
								var m = date.getMonth()+1;
								var d = date.getDate();
								return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
							}
							function myparser(s){
								if (!s) return new Date();
								var ss = (s.split('-'));
								var y = parseInt(ss[0],10);
								var m = parseInt(ss[1],10);
								var d = parseInt(ss[2],10);
								if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
									return new Date(y,m-1,d);
								} else {
									return new Date();
								}
							}
						</script>
						<tr>
							<td>开始时间：</td>
							<td>
								    <input name="fbstart" id="ss" class="easyui-timespinner" style="width:200px;height:34px;padding:10px"
							        required="required" data-options="min:'08:30',showSeconds:true,showSeconds:false">
							        <br><br>
							</td>
						</tr>
						<tr>
							<td>考试时长：</td>
							<td>
								<input id="f" name="fbend" class="easyui-numberspinner" data-options="min:30,max:360,required:true" style="width:200px;height:34px;padding:10px">
								</input>
								<!-- <input id="end" name="fbend" class="easyui-datetimebox" style="width:200px;height:34px;padding:10px"> -->分钟<br><br>
							</td>
						</tr>
						<tr align="center">
							<td colspan="2">
								<a class="easyui-linkbutton">
									<input type="submit" value="发布" style="background-color:transparent;border:0px;width:100px;height:30px;">
								</a>
							</td>
						</tr>
					</table>
				</form>		
		</div>
		<div id="pabb">
		科目名称：
			<select id="paccs" class="easyui-combobox"  data-options="editable:false,prompt:'请选择科目名称'"  name="padept" style="width:200px;height:34px;">
				<option value="0"></option>
			   	<% for(CurriInfo l:lists){ %>
			   		<option value="<%=l.getCurId() %>"><%=l.getCurName() %></option>
				<%}%>
			</select>
		试卷名称：
			<input name="paname" id="paname" class="easyui-textbox" prompt="请输入试卷名称"  style="width:200px;height:34px;padding:10px">
			<a href="#" id="pabtn" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">查询</a>
			<a href="javaScript:addTabs('添加试卷','addpaperInfo.jsp')" id="paadd" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">添加</a>
			<a href="#" id="pabtns" class="easyui-linkbutton" data-options="iconCls:'icon-fb',plain:true">发布</a>
		</div>
	<table id="padg"></table>
</body>
</html>