package com.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.alibaba.druid.pool.DruidDataSource;

public class DBUtil {
	
	public static DruidDataSource ds;
	
	static {
		ds=new DruidDataSource();
		
		ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
		ds.setUrl("jdbc:mysql://localhost:3306/DB?serverTimezone=UTC&characterEncoding=utf8");
		ds.setUsername("root");
		ds.setPassword("123456");
		
		ds.setInitialSize(100);
		ds.setMaxActive(10000);
		ds.setMaxWait(1000);
	}
	
	public static Connection getConn() {
		Connection conn=null;
		try {
			conn=ds.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	
	public static void close(ResultSet rs,PreparedStatement ps,Connection conn){
		try{
			if(rs!=null){
				rs.close();
			}
			if(ps!=null){
				ps.close();
			}
			if(conn!=null){
				conn.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		System.out.println(DBUtil.getConn());
	}
}
