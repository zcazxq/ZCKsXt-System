package com.util;

import java.util.ArrayList;

import com.model.*;
import com.model.Class;


public class ModelQuestionInfo {
	private int total;
	private ArrayList<QuestionInfo> rows;
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public ArrayList<QuestionInfo> getRows() {
		return rows;
	}
	public void setRows(ArrayList<QuestionInfo> rows) {
		this.rows = rows;
	}
}
