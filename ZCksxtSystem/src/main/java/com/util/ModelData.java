package com.util;

public class ModelData {
	private String title;
	private ModelDatatwo[] exam;
	public ModelData() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ModelData(String title, ModelDatatwo[] exam) {
		super();
		this.title = title;
		this.exam = exam;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public ModelDatatwo[] getExam() {
		return exam;
	}
	public void setExam(ModelDatatwo[] exam) {
		this.exam = exam;
	}
}
