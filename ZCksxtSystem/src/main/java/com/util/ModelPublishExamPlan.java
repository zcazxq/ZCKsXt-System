package com.util;

import java.util.ArrayList;

import com.model.*;


public class ModelPublishExamPlan {
	private int total;
	private ArrayList<PublishExamPlan> rows;
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public ArrayList<PublishExamPlan> getRows() {
		return rows;
	}
	public void setRows(ArrayList<PublishExamPlan> rows) {
		this.rows = rows;
	}
}
