package com.util;

import java.util.ArrayList;

import com.model.*;
import com.model.Class;


public class ModelPaperInfo {
	private int total;
	private ArrayList<PaperInfo> rows;
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public ArrayList<PaperInfo> getRows() {
		return rows;
	}
	public void setRows(ArrayList<PaperInfo> rows) {
		this.rows = rows;
	}
}
