package com.util;

import java.util.ArrayList;

import com.model.*;
import com.model.Class;


public class ModelCurriInfo {
	private int total;
	private ArrayList<CurriInfo> rows;
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public ArrayList<CurriInfo> getRows() {
		return rows;
	}
	public void setRows(ArrayList<CurriInfo> rows) {
		this.rows = rows;
	}
}
