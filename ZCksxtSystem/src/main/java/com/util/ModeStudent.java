package com.util;

import java.util.ArrayList;

import com.model.*;
import com.model.Class;


public class ModeStudent {
	private int total;
	private ArrayList<StudentInfo> rows;
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public ArrayList<StudentInfo> getRows() {
		return rows;
	}
	public void setRows(ArrayList<StudentInfo> rows) {
		this.rows = rows;
	}
}
