package com.util;

import java.util.ArrayList;

import com.model.*;
import com.model.Class;


public class Mode {
	private int total;
	private ArrayList<Class> rows;
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public ArrayList<Class> getRows() {
		return rows;
	}
	public void setRows(ArrayList<Class> rows) {
		this.rows = rows;
	}
}
