package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.model.AnswerRecord;
import com.util.DBUtil;

public class AnswerRecordDao {
	Connection conn;
	PreparedStatement ps;
	ResultSet rs;
	
	public int insert(AnswerRecord ar) {
		int row=-1;
		conn=DBUtil.getConn();
		try {
			ps=conn.prepareStatement("insert into AnswerRecord values(null,?,?,?,?,1)");
			ps.setInt(1, ar.getStuId());
			ps.setInt(2, ar.getPaperId());
			ps.setInt(3, ar.getTimuId());
			ps.setString(4, ar.getZXanswer());
			row=ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DBUtil.close(rs, ps, conn);
		}
		return row;
	}
}
