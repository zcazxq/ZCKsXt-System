package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.model.PaperTitleInfo;
import com.util.DBUtil;

public class PaperTitleInfoDao {
	Connection conn;
	PreparedStatement ps;
	ResultSet rs;
	
	public int insert(PaperTitleInfo pi) {
		int row=-1;
		conn=DBUtil.getConn();
		try {
			ps=conn.prepareStatement("insert into paperTitleInfo values(null,?,?,1)");
			ps.setInt(1, pi.getPaperId());
			ps.setInt(2, pi.getTimuId());
			row=ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DBUtil.close(rs, ps, conn);
		}
		return row;
	}
	
	public int delete(PaperTitleInfo pi) {
		int row=-1;
		conn=DBUtil.getConn();
		try {
			ps=conn.prepareStatement("delete from paperTitleInfo where paperId=? and timuid=?");
			ps.setInt(1, pi.getPaperId());
			ps.setInt(2, pi.getTimuId());
			row=ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DBUtil.close(rs, ps, conn);
		}
		return row;
	}
	
	public List<Integer> selectAlltuidBySjid(int sjid) {
		List<Integer> list=new ArrayList<Integer>();
		conn=DBUtil.getConn();
		try {
			ps=conn.prepareStatement("select * from paperTitleInfo where paperid=?");
			ps.setInt(1, sjid);
			rs=ps.executeQuery();
			while(rs.next()) {
				list.add(rs.getInt(3));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DBUtil.close(rs, ps, conn);
		}
		return  list;
	}
}
