package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.model.StudentInfo;
import com.model.UserInfo;
import com.util.DBUtil;

public class UserInfoDao {
	Connection conn;
	PreparedStatement ps;
	ResultSet rs;
	
	public int login(UserInfo ui) {
		int row=-1;
		conn=DBUtil.getConn();
		try {
			ps=conn.prepareStatement("select count(*) from userInfo where userName=? and UserPwd=? and isdelete=1;");
			ps.setString(1, ui.getUserName());
			ps.setString(2, ui.getUserPwd());
			rs=ps.executeQuery();
			if(rs.next()) {
				row=rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DBUtil.close(rs, ps, conn);
		}
		return row;
	}
}
