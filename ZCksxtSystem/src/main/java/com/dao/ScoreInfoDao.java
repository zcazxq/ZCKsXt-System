package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.model.ScoreInfo;
import com.util.DBUtil;

public class ScoreInfoDao {
	Connection conn;
	PreparedStatement ps;
	ResultSet rs;
	
	public int insert(ScoreInfo s) {
		int row=-1;
		conn=DBUtil.getConn();
		try {
			ps=conn.prepareStatement("insert into scoreinfo values(null,?,?,?,1)");
			ps.setInt(1, s.getStuId());
			ps.setInt(2, s.getPaperId());
			ps.setString(3, s.getScore());
			row=ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DBUtil.close(rs, ps, conn);
		}
		return row;
	}
	
	public int selectcountBySidPid(ScoreInfo s) {
		int row=0;
		conn=DBUtil.getConn();
		try {
			ps=conn.prepareStatement("select count(*) from scoreInfo where stuid=? and paperid=?");
			ps.setInt(1, s.getStuId());
			ps.setInt(2, s.getPaperId());
			rs=ps.executeQuery();
			if(rs.next()) {
				row=rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DBUtil.close(rs, ps, conn);
		}
		return row;
	}
	
	public String selectCjBySco(ScoreInfo s) {
		String row="";
		conn=DBUtil.getConn();
		try {
			ps=conn.prepareStatement("select score from scoreInfo where stuid=? and paperid=?");
			ps.setInt(1, s.getStuId());
			ps.setInt(2, s.getPaperId());
			rs=ps.executeQuery();
			if(rs.next()) {
				row=rs.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DBUtil.close(rs, ps, conn);
		}
		return row;
	}
}
