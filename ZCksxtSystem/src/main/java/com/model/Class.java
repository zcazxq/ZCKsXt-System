package com.model;

public class Class {
	 private int ClassId;
	 private String ClassName;
	public int getClassId() {
		return ClassId;
	}
	public void setClassId(int classId) {
		ClassId = classId;
	}
	public String getClassName() {
		return ClassName;
	}
	public void setClassName(String className) {
		ClassName = className;
	}
	public Class(int classId, String className) {
		super();
		ClassId = classId;
		ClassName = className;
	}
	public Class() {
		super();
		// TODO Auto-generated constructor stub
	}
}
