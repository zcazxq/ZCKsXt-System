package com.model;

public class ScoreInfo {
	 private int ScoreId;
	 private int  StuId;
	 private int  PaperId;
	 private String  Score;
	public ScoreInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ScoreInfo(int scoreId, int stuId, int paperId, String score) {
		super();
		ScoreId = scoreId;
		StuId = stuId;
		PaperId = paperId;
		Score = score;
	}
	public int getScoreId() {
		return ScoreId;
	}
	public void setScoreId(int scoreId) {
		ScoreId = scoreId;
	}
	public int getStuId() {
		return StuId;
	}
	public void setStuId(int stuId) {
		StuId = stuId;
	}
	public int getPaperId() {
		return PaperId;
	}
	public void setPaperId(int paperId) {
		PaperId = paperId;
	}
	public String getScore() {
		return Score;
	}
	public void setScore(String score) {
		Score = score;
	}
}
