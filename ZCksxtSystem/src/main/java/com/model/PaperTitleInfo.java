package com.model;

public class PaperTitleInfo {
	 private int PaperTitleId;
	 private int PaperId;
	 private int TimuId;
	 private int IsDelete;
	public PaperTitleInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PaperTitleInfo(int paperTitleId, int paperId, int timuId, int isDelete) {
		super();
		PaperTitleId = paperTitleId;
		PaperId = paperId;
		TimuId = timuId;
		IsDelete = isDelete;
	}
	public int getPaperTitleId() {
		return PaperTitleId;
	}
	public void setPaperTitleId(int paperTitleId) {
		PaperTitleId = paperTitleId;
	}
	public int getPaperId() {
		return PaperId;
	}
	public void setPaperId(int paperId) {
		PaperId = paperId;
	}
	public int getTimuId() {
		return TimuId;
	}
	public void setTimuId(int timuId) {
		TimuId = timuId;
	}
	public int getIsDelete() {
		return IsDelete;
	}
	public void setIsDelete(int isDelete) {
		IsDelete = isDelete;
	}
}
