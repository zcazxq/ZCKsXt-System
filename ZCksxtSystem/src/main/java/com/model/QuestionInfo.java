package com.model;

import java.util.List;

public class QuestionInfo {
	   private int TimuId;
	   private int CurId;
	   private String TimuName;
	   private String A;
	   private String B;
	   private String C;
	   private String D;
	   private String Yes;
	   private String JieXi;
	   private String FenZhi;
	   private int State;
	   private int IsDelete;
	   private String CurName;
	   private int type;
	   private String questionStem;
	   private String[] options;
	   private String[] answer;
       private String analysis;
       private int paperId;
       private String jgsj;
       private String paperName;
	public String getPaperName() {
		return paperName;
	}
	public void setPaperName(String paperName) {
		this.paperName = paperName;
	}
	public String getJgsj() {
		return jgsj;
	}
	public void setJgsj(String jgsj) {
		this.jgsj = jgsj;
	}
	public int getPaperId() {
		return paperId;
	}
	public void setPaperId(int paperId) {
		this.paperId = paperId;
	}
	public String[] getAnswer() {
		return answer;
	}
	public void setAnswer(String[] answer) {
		this.answer = answer;
	}
	public String getAnalysis() {
		return analysis;
	}
	public void setAnalysis(String analysis) {
		this.analysis = analysis;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getQuestionStem() {
		return questionStem;
	}
	public void setQuestionStem(String questionStem) {
		this.questionStem = questionStem;
	}
	public String[] getOptions() {
		return options;
	}
	public void setOptions(String[] options) {
		this.options = options;
	}
	public QuestionInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public QuestionInfo(int timuId, int curId, String timuName, String a, String b, String c, String d, String yes,
			String jieXi, String fenZhi, int isDelete, String curName) {
		super();
		TimuId = timuId;
		CurId = curId;
		TimuName = timuName;
		A = a;
		B = b;
		C = c;
		D = d;
		Yes = yes;
		JieXi = jieXi;
		FenZhi = fenZhi;
		IsDelete = isDelete;
		CurName = curName;
	}
	public int getState() {
		return State;
	}
	public void setState(int state) {
		State = state;
	}
	public String getCurName() {
		return CurName;
	}
	public void setCurName(String curName) {
		CurName = curName;
	}
	public int getTimuId() {
		return TimuId;
	}
	public void setTimuId(int timuId) {
		TimuId = timuId;
	}
	public int getCurId() {
		return CurId;
	}
	public void setCurId(int curId) {
		CurId = curId;
	}
	public String getTimuName() {
		return TimuName;
	}
	public void setTimuName(String timuName) {
		TimuName = timuName;
	}
	public String getA() {
		return A;
	}
	public void setA(String a) {
		A = a;
	}
	public String getB() {
		return B;
	}
	public void setB(String b) {
		B = b;
	}
	public String getC() {
		return C;
	}
	public void setC(String c) {
		C = c;
	}
	public String getD() {
		return D;
	}
	public void setD(String d) {
		D = d;
	}
	public String getYes() {
		return Yes;
	}
	public void setYes(String yes) {
		Yes = yes;
	}
	public String getJieXi() {
		return JieXi;
	}
	public void setJieXi(String jieXi) {
		JieXi = jieXi;
	}
	public String getFenZhi() {
		return FenZhi;
	}
	public void setFenZhi(String fenZhi) {
		FenZhi = fenZhi;
	}
	public int getIsDelete() {
		return IsDelete;
	}
	public void setIsDelete(int isDelete) {
		IsDelete = isDelete;
	}
}
