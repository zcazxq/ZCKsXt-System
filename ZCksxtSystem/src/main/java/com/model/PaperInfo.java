package com.model;

public class PaperInfo {
	   private int PaperId;
	   private int CurId;
	   private String PaperName;
	   private String CreateTime;
	   private int State;
	   private int IsDelete;
	   private String CurName;
	public PaperInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PaperInfo(int paperId, int curId, String paperName, String createTime, int state, int isDelete,
			String curName) {
		super();
		PaperId = paperId;
		CurId = curId;
		PaperName = paperName;
		CreateTime = createTime;
		State = state;
		IsDelete = isDelete;
		CurName = curName;
	}
	public String getCurName() {
		return CurName;
	}
	public void setCurName(String curName) {
		CurName = curName;
	}
	public int getPaperId() {
		return PaperId;
	}
	public void setPaperId(int paperId) {
		PaperId = paperId;
	}
	public int getCurId() {
		return CurId;
	}
	public void setCurId(int curId) {
		CurId = curId;
	}
	public String getPaperName() {
		return PaperName;
	}
	public void setPaperName(String paperName) {
		PaperName = paperName;
	}
	public String getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}
	public int getState() {
		return State;
	}
	public void setState(int state) {
		State = state;
	}
	public int getIsDelete() {
		return IsDelete;
	}
	public void setIsDelete(int isDelete) {
		IsDelete = isDelete;
	}
	
}
