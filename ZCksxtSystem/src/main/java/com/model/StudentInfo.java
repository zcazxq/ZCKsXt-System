package com.model;

public class StudentInfo {
	   private int StuId;
	   private int ClassId;
	   private String StuName;
	   private String StuPwd;
	   private String StuSex;
	   private String StuPhone;
	   private String StuStatus;
	   private String ClassName;
	public StudentInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public StudentInfo(int stuId, int classId, String stuName, String stuPwd, String stuSex, String stuPhone,
			String stuStatus, String className) {
		super();
		StuId = stuId;
		ClassId = classId;
		StuName = stuName;
		StuPwd = stuPwd;
		StuSex = stuSex;
		StuPhone = stuPhone;
		StuStatus = stuStatus;
		ClassName = className;
	}
	public String getClassName() {
		return ClassName;
	}
	public void setClassName(String className) {
		ClassName = className;
	}
	public int getStuId() {
		return StuId;
	}
	public void setStuId(int stuId) {
		StuId = stuId;
	}
	public int getClassId() {
		return ClassId;
	}
	public void setClassId(int classId) {
		ClassId = classId;
	}
	public String getStuName() {
		return StuName;
	}
	public void setStuName(String stuName) {
		StuName = stuName;
	}
	public String getStuPwd() {
		return StuPwd;
	}
	public void setStuPwd(String stuPwd) {
		StuPwd = stuPwd;
	}
	public String getStuSex() {
		return StuSex;
	}
	public void setStuSex(String stuSex) {
		StuSex = stuSex;
	}
	public String getStuPhone() {
		return StuPhone;
	}
	public void setStuPhone(String stuPhone) {
		StuPhone = stuPhone;
	}
	public String getStuStatus() {
		return StuStatus;
	}
	public void setStuStatus(String stuStatus) {
		StuStatus = stuStatus;
	}
}
