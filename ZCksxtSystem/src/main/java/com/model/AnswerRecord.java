package com.model;

public class AnswerRecord {
	   private int AnswerRecordId;
	   private int StuId;
	   private int PaperId;
	   private int TimuId;
	   private String ZXanswer;
	public AnswerRecord() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AnswerRecord(int answerRecordId, int stuId, int paperId, int timuId, String zXanswer) {
		super();
		AnswerRecordId = answerRecordId;
		StuId = stuId;
		PaperId = paperId;
		TimuId = timuId;
		ZXanswer = zXanswer;
	}
	public int getAnswerRecordId() {
		return AnswerRecordId;
	}
	public void setAnswerRecordId(int answerRecordId) {
		AnswerRecordId = answerRecordId;
	}
	public int getStuId() {
		return StuId;
	}
	public void setStuId(int stuId) {
		StuId = stuId;
	}
	public int getPaperId() {
		return PaperId;
	}
	public void setPaperId(int paperId) {
		PaperId = paperId;
	}
	public int getTimuId() {
		return TimuId;
	}
	public void setTimuId(int timuId) {
		TimuId = timuId;
	}
	public String getZXanswer() {
		return ZXanswer;
	}
	public void setZXanswer(String zXanswer) {
		ZXanswer = zXanswer;
	}
}
