package com.model;

public class PublishExamPlan {
	   private int publishExamPlanId;
	   private int PaperId;
	   private int ClassId;
	   private String StartTime;
	   private String EndTime;
	   private String CreatTime;
	   private String PaperName;
	   private String ClassName;
	   private String sj;
	   private int tscount;
	   private int fz;
	public PublishExamPlan() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PublishExamPlan(int publishExamPlanId, int paperId, int classId, String startTime, String endTime,
			String creatTime) {
		super();
		this.publishExamPlanId = publishExamPlanId;
		PaperId = paperId;
		ClassId = classId;
		StartTime = startTime;
		EndTime = endTime;
		CreatTime = creatTime;
	}
	public String getSj() {
		return sj;
	}
	public void setSj(String sj) {
		this.sj = sj;
	}
	public int getTscount() {
		return tscount;
	}
	public void setTscount(int tscount) {
		this.tscount = tscount;
	}
	public int getFz() {
		return fz;
	}
	public void setFz(int fz) {
		this.fz = fz;
	}
	public String getClassName() {
		return ClassName;
	}
	public void setClassName(String className) {
		ClassName = className;
	}
	public String getPaperName() {
		return PaperName;
	}
	public void setPaperName(String paperName) {
		PaperName = paperName;
	}
	public int getPublishExamPlanId() {
		return publishExamPlanId;
	}
	public void setPublishExamPlanId(int publishExamPlanId) {
		this.publishExamPlanId = publishExamPlanId;
	}
	public int getPaperId() {
		return PaperId;
	}
	public void setPaperId(int paperId) {
		PaperId = paperId;
	}
	public int getClassId() {
		return ClassId;
	}
	public void setClassId(int classId) {
		ClassId = classId;
	}
	public String getStartTime() {
		return StartTime;
	}
	public void setStartTime(String startTime) {
		StartTime = startTime;
	}
	public String getEndTime() {
		return EndTime;
	}
	public void setEndTime(String endTime) {
		EndTime = endTime;
	}
	public String getCreatTime() {
		return CreatTime;
	}
	public void setCreatTime(String creatTime) {
		CreatTime = creatTime;
	}
}
