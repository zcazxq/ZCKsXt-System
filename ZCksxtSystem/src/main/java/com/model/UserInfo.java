package com.model;

public class UserInfo {
	private int UserId;
	private String UserName;
	private String UserPwd;
	public UserInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserInfo(int userId, String userName, String userPwd) {
		super();
		UserId = userId;
		UserName = userName;
		UserPwd = userPwd;
	}
	public int getUserId() {
		return UserId;
	}
	public void setUserId(int userId) {
		UserId = userId;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getUserPwd() {
		return UserPwd;
	}
	public void setUserPwd(String userPwd) {
		UserPwd = userPwd;
	}
}
