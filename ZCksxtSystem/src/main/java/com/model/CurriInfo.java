package com.model;

public class CurriInfo {
	 private int CurId;
	 private String CurName;
	 private int IsDelete;
	public CurriInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CurriInfo(int curId, String curName, int isDelete) {
		super();
		CurId = curId;
		CurName = curName;
		IsDelete = isDelete;
	}
	public int getCurId() {
		return CurId;
	}
	public void setCurId(int curId) {
		CurId = curId;
	}
	public String getCurName() {
		return CurName;
	}
	public void setCurName(String curName) {
		CurName = curName;
	}
	public int getIsDelete() {
		return IsDelete;
	}
	public void setIsDelete(int isDelete) {
		IsDelete = isDelete;
	}
}
