package com.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.dao.PaperInfoDao;
import com.dao.PublishExamPlanDao;
import com.dao.QuestionInfoDao;
import com.dao.ScoreInfoDao;
import com.model.PublishExamPlan;
import com.model.ScoreInfo;
import com.model.StudentInfo;

/**
 * Servlet implementation class QianTanServlet
 */
@WebServlet("/QianTaiServlet")
public class QianTaiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PublishExamPlanDao ped=new PublishExamPlanDao();
	ScoreInfoDao sid=new ScoreInfoDao();
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String op=request.getParameter("op");
		
		if("kaosi".equals(op)) {
			int bjid=Integer.parseInt(request.getParameter("bjid"));
			ArrayList<PublishExamPlan> list=ped.selectAllByBjid(bjid);
			for(int i=0;i<list.size();i++) {
				
				
				String start=list.get(i).getStartTime();
				String end=list.get(i).getEndTime();
				list.get(i).setTscount(ped.selectsjts(list.get(i).getPaperId()));
				list.get(i).setFz(2);
//				
//				Date star = new Date(start);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateStr = start;
				String dateend = end;
				Date dates = null;
				Date ends = null;
				try {
					dates = sdf.parse(dateStr);
					ends=sdf.parse(dateend);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				long jg=ends.getTime()-dates.getTime();
				double days = jg / 1000 / 60 / 60 / 24;
				
				double daysRound = Math.floor(days);
				
				double hours = jg/ 1000 / 60 / 60 - (24 * daysRound);
				
				double hoursRound = Math.floor(hours);
				
				double minutes = jg / 1000 /60 - (24 * 60 * daysRound) - (60 * hoursRound);
				
				double minutesRound = Math.floor(minutes);
				
				double seconds = jg/ 1000 - (24 * 60 * 60 * daysRound) - (60 * 60 * hoursRound) - (60 * minutesRound);
				
				double secondsRound = Math.floor(seconds);
				int hoursRounds=(int)hoursRound;
				int minutesRounds=(int)minutesRound;
				int secondsRounds=(int)secondsRound;
				
				var time = hoursRounds +":"+minutesRounds+":"+secondsRounds;
				list.get(i).setSj(hoursRounds*60+minutesRounds+"");
			}
			System.out.println(JSON.toJSONString(list));
			request.getSession().setAttribute("list", list);
			response.getWriter().print("success");
		}else if("kaishiks".equals(op)) {
			PaperInfoDao pid=new PaperInfoDao();
			QuestionInfoDao qid=new QuestionInfoDao();
			int id=pid.selectIdBynames(request.getParameter("sjname"));
			int stuid=((StudentInfo)request.getSession().getAttribute("model")).getStuId();
			ScoreInfo si=new ScoreInfo();
			si.setPaperId(id);
			si.setStuId(stuid);
			
			if(sid.selectcountBySidPid(si)==1) {
				response.getWriter().print("您已参加过考试了！");
			}else {
				request.getSession().setAttribute("qu",qid.selectShiJue(id));
				response.getWriter().print("success");
			}
		}else if("cxcj".equals(op)) {
			int sjid=Integer.parseInt(request.getParameter("sjid"));
			int stuid=((StudentInfo)request.getSession().getAttribute("model")).getStuId();
			ScoreInfo si=new ScoreInfo();
			si.setPaperId(sjid);
			si.setStuId(stuid);
			response.getWriter().print(sid.selectCjBySco(si));
		}
		
	}

}
