package com.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.dao.*;
import com.util.*;
import com.model.Class;

/**
 * Servlet implementation class ClassServlet
 */
@WebServlet("/CurriInfoServlet")
public class CurriInfoServlet extends HttpServlet {
	CurriInfoDao cid=new CurriInfoDao();
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String op=request.getParameter("op");
		
		if("add".equals(op)) {
			String name=request.getParameter("namecur");
			int pd=0;
			if(cid.selectByname(name)==1) {
				pd=cid.insertAll(name,"修改");
			}else {
				pd=cid.insertAll(name,"添加");
			}
			
			if(pd==1) {
				response.getWriter().print("添加成功");
			}else{
				response.getWriter().print("添加失败,该班级可能已存在,");
			}
		}else if("update".equals(op)){
			String id=request.getParameter("id");
			String name=request.getParameter("name");
			Class c=new Class(Integer.parseInt(id), name);
			if(cid.updateById(c)==1) {
				response.getWriter().print("修改成功");
			}else{
				response.getWriter().print("修改失败");
			}
		}else if("delete".equals(op)){
			if(cid.deleteById(Integer.parseInt(request.getParameter("id")))==1) {
				response.getWriter().print("删除成功");
			}else{
				response.getWriter().print("删除失败");
			}
		}else if("select".equals(op)){
			String rows=request.getParameter("rows");
			String page=request.getParameter("page");
			String name=request.getParameter("name");
			
			ModelCurriInfo list=cid.selectAll(Integer.parseInt(page), Integer.parseInt(rows), name);
			response.getWriter().print(JSON.toJSONString(list));
		}
		
	}

}
