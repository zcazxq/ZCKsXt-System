package com.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.dao.ClassDao;
import com.model.Class;
import com.model.StudentInfo;
import com.util.Mode;

/**
 * Servlet implementation class ClassServlet
 */
@WebServlet("/ExaminationServlet")
public class ExaminationServlet extends HttpServlet {
	ClassDao cd=new ClassDao();
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String op=request.getParameter("op");
		
		if("update".equals(op)){
			String id=request.getParameter("id");
			String name=request.getParameter("name");
			Class c=new Class(Integer.parseInt(id), name);
			if(cd.updateById(c)==1) {
				response.getWriter().print("修改成功");
			}else{
				response.getWriter().print("修改失败");
			}
		}else if("delete".equals(op)){
			if(cd.deleteById(Integer.parseInt(request.getParameter("id")))==1) {
				response.getWriter().print("删除成功");
			}else{
				response.getWriter().print("删除失败");
			}
		}else if("select".equals(op)){
			String rows=request.getParameter("rows");
			String page=request.getParameter("page");
			String name=request.getParameter("name");
			
			Mode list=cd.selectAll(Integer.parseInt(page), Integer.parseInt(rows), name);
			response.getWriter().print(JSON.toJSONString(list));
		}else if("selectAll".equals(op)) {
			ArrayList<Class> list=cd.selectAll();
			response.getWriter().print(JSON.toJSONString(list));
		}
		
	}

}
