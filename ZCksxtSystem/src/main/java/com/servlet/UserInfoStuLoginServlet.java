package com.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentInfoDao;
import com.dao.UserInfoDao;
import com.model.StudentInfo;
import com.model.UserInfo;

/**
 * Servlet implementation class UserInfoStuLoginServlet
 */
@WebServlet("/UserInfoStuLoginServlet")
public class UserInfoStuLoginServlet extends HttpServlet {
	StudentInfoDao sid=new StudentInfoDao();
	UserInfoDao uid=new UserInfoDao();
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		int op=Integer.parseInt(request.getParameter("op"));
		if(op==0) {
			StudentInfo stu=new StudentInfo();
			stu.setStuStatus(request.getParameter("name"));
			stu.setStuPwd(request.getParameter("pwd"));
			if(sid.login(stu)==1) {
				request.getSession().setAttribute("model", sid.selectAlloneByNamePwd(stu));
				response.getWriter().print(((StudentInfo)request.getSession().getAttribute("model")).getClassId());
			}else {
				response.getWriter().print("账号或密码有误！");
			}
		}else if(op==1) {
			UserInfo ui=new UserInfo();
			ui.setUserName(request.getParameter("name"));
			ui.setUserPwd(request.getParameter("pwd"));
			if(uid.login(ui)==1) {
				response.getWriter().print("登陆成功");
			}else {
				response.getWriter().print("账号或密码有误！");
			}
		}
	}

}
