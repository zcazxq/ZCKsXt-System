package com.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.dao.PublishExamPlanDao;
import com.util.ModelPublishExamPlan;

/**
 * Servlet implementation class PublishExamPlanServlet
 */
@WebServlet("/PublishExamPlanServlet")
public class PublishExamPlanServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PublishExamPlanDao ped=new PublishExamPlanDao();

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String op=request.getParameter("op");

		if("select".equals(op)) {
			int page=Integer.parseInt(request.getParameter("page"));
			int rows=Integer.parseInt(request.getParameter("rows"));
			String name=request.getParameter("name");
			if(name!=null&&name.equals("0")) {
				name="";
			}
			
			ModelPublishExamPlan list=ped.selectAll(page, rows, name);
			response.getWriter().print(JSON.toJSONString(list));
		}else if("delete".equals(op)){
			int id=Integer.parseInt(request.getParameter("id"));
			if(ped.deleteByid(id)==1) {
				response.getWriter().print("删除成功");
			}else {
				response.getWriter().print("删除失败");
			}
		}
	}

}
