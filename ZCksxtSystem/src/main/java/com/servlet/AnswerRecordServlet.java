package com.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.AnswerRecordDao;
import com.dao.ScoreInfoDao;
import com.model.AnswerRecord;
import com.model.ScoreInfo;
import com.model.StudentInfo;

/**
 * Servlet implementation class AnswerRecordServlet
 */
@WebServlet("/AnswerRecordServlet")
public class AnswerRecordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	AnswerRecordDao ard=new AnswerRecordDao();
	ScoreInfoDao sid=new ScoreInfoDao();
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String op=request.getParameter("op");
		
		if("jiaojuan".equals(op)) {
			int stuid=((StudentInfo)request.getSession().getAttribute("model")).getStuId();
			int lengh=((List)request.getSession().getAttribute("qu")).size();
			int count=0;
			AnswerRecord a=new AnswerRecord();
			a.setStuId(stuid);
			a.setPaperId(Integer.parseInt(request.getParameter("sjid")));
			for(int i=1;i<=lengh;i++) {
				a.setTimuId(i);
				a.setZXanswer(request.getParameter(i+""));
				if(ard.insert(a)==1) {
					count++;
				}
			}
			ScoreInfo si=new ScoreInfo();
			si.setStuId(stuid);
			si.setPaperId(a.getPaperId());
			si.setScore(request.getParameter("cj"));
			if(sid.insert(si)==1) {
			
				if(count==lengh) {
					response.getWriter().print("交卷成功");
				}else {
					response.getWriter().print("信息有误");
				}
			}
		}
	}

}
